package Pruebas;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import dao.ConfiguracionPOP3DAO;
import dao.ConfiguracionSMTPDAO;
import dao.ContactoDAO;
import dao.impl.ConfiguracionPop3DaoImpl;
import dao.impl.ConfiguracionSmtpDaoImpl;
import dao.impl.ContactoDaoH2Impl;
import exceptions.DAOException;
import modelo.ConfiguracionPOP3;
import modelo.ConfiguracionSMTP;
import modelo.Contacto;

public class PruebaConfiguracion 
{
	//POP3
	public static void probarGuardarConfiguracionPOP3(ConfiguracionPOP3 unConfiguracionPOP3)
	{
		try 
		{
			//PERSISTENCIA DE CONFIGURACION POP3
            ConfiguracionPOP3DAO dao = new ConfiguracionPop3DaoImpl();
            dao.crearConfiguracionPop3(unConfiguracionPOP3);
            JOptionPane.showMessageDialog(null, "Se guardo con exito la configuracion POP3");
        } 
		catch (DAOException daoe) 
		{
            JOptionPane.showMessageDialog(null, daoe.getMessage());
        }
	}

	public static void probarModificarConfiguracionPOP3(ConfiguracionPOP3 unConfiguracionPOP3)
	{
		ConfiguracionPOP3 modificado = unConfiguracionPOP3;
		modificado.setHost(modificado.getHost()+"_mod");
		modificado.setPuerto(modificado.getPuerto()+"_mod");
		modificado.setStarttls(modificado.getStarttls()+"_mod");
		
		try 
		{
			//PERSISTENCIA DE CONFIGURACION POP3
            ConfiguracionPOP3DAO dao = new ConfiguracionPop3DaoImpl();
            dao.modificarConfiguracionPop3(modificado);
            JOptionPane.showMessageDialog(null, "Se Modifico la Configuracion POP3 con exito");
        } 
		catch (DAOException daoe) 
		{
            JOptionPane.showMessageDialog(null, daoe.getMessage());
        }
	}

	public static void probarTraerTodosConfiguracionPOP3()
	{
		try 
		{
			//PERSISTENCIA DE CONFIGURACION POP3
			ConfiguracionPOP3DAO dao = new ConfiguracionPop3DaoImpl();
			ConfiguracionPOP3 unaConfig = dao.consultarConfiguracionPop3();
            JOptionPane.showMessageDialog(null, "Se trajo bien la Configuracion POP3 con exito.\n"+unaConfig.toString());
        } 
		catch (DAOException daoe) 
		{
            JOptionPane.showMessageDialog(null, daoe.getMessage());
        }
	}

	//SMTP

	public static void probarGuardarConfiguracionSMTP(ConfiguracionSMTP unConfiguracionSMTP)
	{
		try 
		{
			//PERSISTENCIA DE CONFIGURACION SMTP
            ConfiguracionSMTPDAO dao = new ConfiguracionSmtpDaoImpl();
            dao.crearConfiguracionSmtp(unConfiguracionSMTP);
            JOptionPane.showMessageDialog(null, "Se guardo con exito la configuracion SMTP");
        } 
		catch (DAOException daoe) 
		{
            JOptionPane.showMessageDialog(null, daoe.getMessage());
        }
	}

	public static void probarModificarConfiguracionSMTP(ConfiguracionSMTP unConfiguracionSMTP)
	{
		ConfiguracionSMTP modificado = unConfiguracionSMTP;
		modificado.setHost(modificado.getHost()+"_mod");
		modificado.setPuerto(modificado.getPuerto()+"_mod");
		modificado.setStarttls(modificado.getStarttls()+"_mod");
		modificado.setAutorizacion(modificado.getAutorizacion()+"_mod");
		
		try 
		{
			//PERSISTENCIA DE CONFIGURACION SMTP
			ConfiguracionSMTPDAO dao = new ConfiguracionSmtpDaoImpl();
            dao.modificarConfiguracionSmtp(modificado);
            JOptionPane.showMessageDialog(null, "Se Modifico la Configuracion SMTP con exito");
        } 
		catch (DAOException daoe) 
		{
            JOptionPane.showMessageDialog(null, daoe.getMessage());
        }
	}

	public static void probarTraerTodosConfiguracionSMTP()
	{
		try 
		{
			//PERSISTENCIA DE CONFIGURACION SMTP
			ConfiguracionSMTPDAO dao = new ConfiguracionSmtpDaoImpl();
			ConfiguracionSMTP unaConfig = dao.consultarConfiguracionSmtp();
            JOptionPane.showMessageDialog(null, "Se trajo bien la Configuracion SMTP con exito.\n"+unaConfig.toString());
        } 
		catch (DAOException daoe) 
		{
            JOptionPane.showMessageDialog(null, daoe.getMessage());
        }
	}

}
