package Pruebas;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import dao.ContactoDAO;
import dao.ContactoDAO;
import dao.impl.ContactoDaoH2Impl;
import dao.impl.ContactoDaoH2Impl;
import exceptions.DAOException;
import modelo.Contacto;
import modelo.Contacto;

public class PruebaContacto 
{
	public static void probarGuardarContacto(Contacto unContacto)
	{
		try 
		{
			//PERSISTENCIA DE CONTACTOS
            ContactoDAO dao = new ContactoDaoH2Impl();
            dao.crearContacto(unContacto);
            JOptionPane.showMessageDialog(null, "Se guardo el contacto con exito");
        } 
		catch (DAOException daoe) 
		{
            JOptionPane.showMessageDialog(null, daoe.getMessage());
        }
	}
	
	public static void probarEliminarContacto(Contacto unContacto)
	{
		try 
		{
			//PERSISTENCIA DE USUARIO
			ContactoDAO dao = new ContactoDaoH2Impl();
            dao.borrarContacto(unContacto);
            JOptionPane.showMessageDialog(null, "Se borro el contacto con exito");
        } 
		catch (DAOException daoe) 
		{
            JOptionPane.showMessageDialog(null, daoe.getMessage());
        }
	}

	public static void probarModificarContacto(Contacto unContacto)
	{
		Contacto modificado = unContacto;
		modificado.setApellido(modificado.getApellido()+"_mod");
		modificado.setNombre(modificado.getNombre()+"_mod");
		modificado.setDireccion(modificado.getDireccion()+"_mod");
		
		try 
		{
			//PERSISTENCIA DE USUARIO

            ContactoDAO dao = new ContactoDaoH2Impl();
            dao.modificarContacto(modificado);
            JOptionPane.showMessageDialog(null, "Se Modifico el contacto con exito");
        } 
		catch (DAOException daoe) 
		{
            JOptionPane.showMessageDialog(null, daoe.getMessage());
        }
	}

	public static void probarTraerTodosContactos()
	{
		try 
		{
			//PERSISTENCIA DE USUARIO

            ContactoDAO dao = new ContactoDaoH2Impl();
            ArrayList<Contacto> listaContacto = dao.listarContactos();
            String contactosString = "";
            for (Contacto contacto : listaContacto) {
				contactosString += contacto.toString()+"\n";
			}
            JOptionPane.showMessageDialog(null, "Se listaron los contactos con exito.\n"+contactosString);
        } 
		catch (DAOException daoe) 
		{
            JOptionPane.showMessageDialog(null, daoe.getMessage());
        }
	}

	public static void probarTraerUnContacto(String nombreContacto)
	{
		try 
		{
			//PERSISTENCIA DE USUARIO

            ContactoDAO dao = new ContactoDaoH2Impl();
            Contacto unContacto = dao.consultarContacto(nombreContacto);
            JOptionPane.showMessageDialog(null, "Se trajo bien el contacto con exito.\n"+unContacto.toString());
        } 
		catch (DAOException daoe) 
		{
            JOptionPane.showMessageDialog(null, daoe.getMessage());
        }
	}
	
}
