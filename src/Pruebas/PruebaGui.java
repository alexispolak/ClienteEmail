package Pruebas;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;

import gui.paneles.PanelBotonera;

public class PruebaGui 
{
	public static void probarBotonera()
	{
		JFrame frame = new JFrame("Prueba Botonera");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        ActionListener accionBotonIzquierdo = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Accion boton izquierdo");
			}
		};
		
		ActionListener accionBotonDerecho = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Accion boton derecho");
			}
		};
       
        PanelBotonera panel = new PanelBotonera();

        frame.getContentPane().add(panel);

        //frame.pack();
        frame.setVisible(true);
	}
}
