package Pruebas;

import modelo.AdministradorMails;
import modelo.Mail;
import modelo.Usuario;

public class PruebaEmail 
{
	public static void probarRecibirMail(AdministradorMails administradorPrincipal, Usuario unUsuario)
	{
		try
		{
			administradorPrincipal.recibir();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void probarEnviarMail(AdministradorMails administradorPrincipal, Usuario unUsuario, Mail unMail)
	{
		try
		{
			administradorPrincipal.enviar(unMail);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
}
