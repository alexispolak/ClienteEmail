package Pruebas;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import dao.UsuarioDAO;
import dao.impl.UsuarioDaoH2Impl;
import exceptions.DAOException;
import modelo.Usuario;

public class PruebaUsuarios 
{

	public static void probarGuardarUsuario(Usuario unUsuario)
	{
		try 
		{
			//PERSISTENCIA DE USUARIO

            UsuarioDAO dao = new UsuarioDaoH2Impl();
            dao.crearUsuario(unUsuario);
            JOptionPane.showMessageDialog(null, "Se guardo el usuario con exito");
        } 
		catch (DAOException daoe) 
		{
            JOptionPane.showMessageDialog(null, daoe.getMessage());
        }
	}

	public static void probarEliminarUsuario(Usuario unUsuario)
	{
		try 
		{
			//PERSISTENCIA DE USUARIO

            UsuarioDAO dao = new UsuarioDaoH2Impl();
            dao.borrarUsuario(unUsuario);
            JOptionPane.showMessageDialog(null, "Se borro el usuario con exito");
        } 
		catch (DAOException daoe) 
		{
            JOptionPane.showMessageDialog(null, daoe.getMessage());
        }
	}

	public static void probarModificarUsuario(Usuario unUsuario)
	{
		Usuario modificado = unUsuario;
		modificado.setApellido(modificado.getApellido()+"_mod");
		modificado.setNombre(modificado.getNombre()+"_mod");
		modificado.setContrasenia(modificado.getContrasenia()+"_mod");
		
		try 
		{
			//PERSISTENCIA DE USUARIO

            UsuarioDAO dao = new UsuarioDaoH2Impl();
            dao.modificarUsuario(modificado);
            JOptionPane.showMessageDialog(null, "Se Modifico el usuario con exito");
        } 
		catch (DAOException daoe) 
		{
            JOptionPane.showMessageDialog(null, daoe.getMessage());
        }
	}

	public static void probarTraerTodosUsuarios()
	{
		try 
		{
			//PERSISTENCIA DE USUARIO

            UsuarioDAO dao = new UsuarioDaoH2Impl();
            ArrayList<Usuario> listaUsuario = dao.listarUsuarios();
            String usuariosString = "";
            for (Usuario usuario : listaUsuario) {
				usuariosString += usuario.toString()+"\n";
			}
            JOptionPane.showMessageDialog(null, "Se listaron los usuarios con exito.\n"+usuariosString);
        } 
		catch (DAOException daoe) 
		{
            JOptionPane.showMessageDialog(null, daoe.getMessage());
        }
	}

	public static void probarTraerUnUsuario(String nombreUsuario)
	{
		try 
		{
			//PERSISTENCIA DE USUARIO

            UsuarioDAO dao = new UsuarioDaoH2Impl();
            Usuario unUsuario = dao.consultarUsuario(nombreUsuario);
            JOptionPane.showMessageDialog(null, "Se trajo bien el usuario con exito.\n"+unUsuario.toString());
        } 
		catch (DAOException daoe) 
		{
            JOptionPane.showMessageDialog(null, daoe.getMessage());
        }
	}
	
}
