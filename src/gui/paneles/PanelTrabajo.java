package gui.paneles;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.sun.mail.smtp.SMTPAddressFailedException;

import exceptions.DAOException;
import exceptions.EnvioFallidoException;
import exceptions.LoginException;
import gui.GuiManager;
import gui.ImagenesManager;
import modelo.Contacto;
import modelo.Mail;


public class PanelTrabajo extends JPanel implements ActionListener
{

	//ATRIBUTOS
	private PanelBotonera panelBotoneraSuperior;
	private PanelVisualizacionTrabajo panelPrincipalVisualizacion;
	
	//CONSTRUCTORES
	public PanelTrabajo()
	{
		super();
		this.armarPanel();
	}

	//GETTERS
	public PanelBotonera getPanelBotoneraSuperior() {
		return panelBotoneraSuperior;
	}
	public void setPanelBotoneraSuperior(PanelBotonera panelBotoneraSuperior) {
		this.panelBotoneraSuperior = panelBotoneraSuperior;
	}
	public PanelVisualizacionTrabajo getPanelPrincipalVisualizacion() {
		return panelPrincipalVisualizacion;
	}
	public void setPanelPrincipalVisualizacion(PanelVisualizacionTrabajo panelMail) {
		this.panelPrincipalVisualizacion = panelMail;
		this.add(this.panelPrincipalVisualizacion, BorderLayout.CENTER);
	}

	//METODOS
	private void armarPanel()
	{
		//Seteo los atributos
		this.panelPrincipalVisualizacion = (PanelVisualizacionTrabajo)GuiManager.unicaInstancia().getPanelVisualizacionMail();
		this.panelBotoneraSuperior = new PanelBotonera(this.accionBotonenviarNuevoCorreo(this),"",this.accionBotonLimpiarCampos(),"",ImagenesManager.unicaInstancia().getIconoBotonEnviar(), ImagenesManager.unicaInstancia().getIconoBotonDescartar());
		
		
		//Establezco el layout
		BorderLayout layoutDelPanel = new BorderLayout();
		this.setLayout(layoutDelPanel);
		
		//Agrego los paneles internos al panel de la clase
		this.add(this.panelBotoneraSuperior, BorderLayout.NORTH);
		this.add(this.panelPrincipalVisualizacion, BorderLayout.CENTER);
		
		//Agrego la referencia al GuiManager de los botones
		GuiManager.unicaInstancia().setBotonEnviar(this.panelBotoneraSuperior.getBotonIzquierdo());
		GuiManager.unicaInstancia().setBotonDescartar(this.panelBotoneraSuperior.getBotonDerecho());
	}
	
	

	public void actualizarVista()
	{
		this.revalidate();
		
		this.repaint();
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
    public Dimension getMinimumSize() {
        return new Dimension(200, 300);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(GuiManager.unicaInstancia().getMarcoAplicacion().getSize().width-675, GuiManager.unicaInstancia().getMarcoAplicacion().getSize().height-50); //50 es el tamanio de la botonera de alto
    }

    @Override
    public void paintComponent(Graphics g) {
        int margin = 10;
        Dimension dim = getSize();
        super.paintComponent(g);
        g.setColor(Color.red);
        g.fillRect(margin, margin, dim.width - margin * 2, dim.height - margin * 2);
    }	
    
    private ActionListener accionBotonenviarNuevoCorreo(PanelTrabajo panelTrabajo)
    {
    	return new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				PanelTrabajo unPanelTrabajo = (PanelTrabajo) GuiManager.unicaInstancia().getPanelPrincipal().getPanelVisualizacion();
				PanelMail unPanelMail = (PanelMail) unPanelTrabajo.getPanelPrincipalVisualizacion();
				
				try 
				{
					Mail nuevoMail = new Mail();
					nuevoMail.setTag("Enviado");
					nuevoMail.setAsunto(((PanelDeParaAsunto)unPanelMail.getPanelDeParaAsunto()).getTextoAsunto().getText());
					nuevoMail.setDestinatario(((PanelDeParaAsunto)unPanelMail.getPanelDeParaAsunto()).getTextoPara().getText());
					nuevoMail.setOrigen(((PanelDeParaAsunto)unPanelMail.getPanelDeParaAsunto()).getTextoDe().getText());
					nuevoMail.setMensaje(((PanelRedaccion)unPanelMail.getPanelRedaccion()).getCampoTexto().getText());
					
					GuiManager.unicaInstancia().getAdministradorMails().enviar(nuevoMail);
						
					Mail.guardarMailEnviado(nuevoMail);
					
					JOptionPane.showMessageDialog(null, "El mail se envio con exito.");

				}
				catch (DAOException daoe) 
				{
					daoe.printStackTrace();
		            JOptionPane.showMessageDialog(null, "Se produjo un problema al guardar el mail.");
				}
				catch ( LoginException le) 
				{
					le.printStackTrace();
		            JOptionPane.showMessageDialog(null, "Se produjo un problema al loguearse.");
				}
				catch (EnvioFallidoException ef) 
				{
					ef.printStackTrace();
		            JOptionPane.showMessageDialog(null, "Se produjo un problema en el envio.\n"+ef.getMessage());
				}
				finally
				{
					unPanelMail.limpiarCampos();
				}
			}
		};
    }
    
    private ActionListener accionBotonLimpiarCampos() 
    {
		return GuiManager.unicaInstancia().getAccionBorrarCamposMail();
	}
}
