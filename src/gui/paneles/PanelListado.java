package gui.paneles;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import gui.GuiManager;
import modelo.Contacto;
import modelo.ContactosTableModel;
import modelo.Mail;
import modelo.MailTableModel;

public class PanelListado extends JPanel implements ActionListener
{

	//ATRIBUTOS
	private JTable tablaMails;
	private MailTableModel modeloTabla;
	private JScrollPane panelConScrollParaTabla;
	private String tagActual;
	
	//CONSTRUCTORES
	public PanelListado()
	{
		super();
		this.armarPanel();
	}
	
	//GETTERS
	public JTable getTablaMails() {
		return tablaMails;
	}
	public void setTablaMails(JTable tablaMails) {
		this.tablaMails = tablaMails;
	}
	public MailTableModel getModeloTabla() {
		return modeloTabla;
	}
	public void setModeloTabla(MailTableModel modeloTabla) {
		this.modeloTabla = modeloTabla;
	}
	public JScrollPane getPanelConScrollParaTabla() {
		return panelConScrollParaTabla;
	}
	public void setPanelConScrollParaTabla(JScrollPane panelConScrollParaTabla) {
		this.panelConScrollParaTabla = panelConScrollParaTabla;
	}
	public String getTagActual() {
		return tagActual;
	}
	public void setTagActual(String tagActual) {
		this.tagActual = tagActual;
	}

	//METODOS
	private void armarPanel()
	{
		//Seteo todos los atributos
		this.modeloTabla = new MailTableModel();
		this.tablaMails = new JTable(this.modeloTabla);
		this.panelConScrollParaTabla = new JScrollPane(this.tablaMails);
		
		//Establezco el layout
		BorderLayout layoutDelPanel = new BorderLayout();
		this.setLayout(layoutDelPanel);
		
		
		this.tablaMails.addMouseListener(new MouseAdapter() 
		{
	        public void mouseClicked(MouseEvent e) 
	        {
	        	ArrayList<Mail> listaMail = new ArrayList<Mail>();
	        	listaMail = (ArrayList<Mail>) modeloTabla.getFilas();
	        	Mail unMailSeleccionado = listaMail.get(tablaMails.getSelectedRow());

	        	GuiManager.unicaInstancia().getAccionMostrarCorreoRecibido(unMailSeleccionado);
	        }
	    });
		
		
		//Le doy el estilo
		this.panelConScrollParaTabla.setBorder(BorderFactory.createEmptyBorder());
		this.panelConScrollParaTabla.setBackground(new Color(0.976f, 0.969f, 0.953f));
		this.tablaMails.setBackground(new Color(0.976f, 0.969f, 0.953f));
		
		this.add(this.panelConScrollParaTabla, BorderLayout.CENTER);
	}
	
	
	public void establecerValores(ArrayList<Mail> listaMails, String tagActual)
	{
		this.setTagActual(tagActual);
		
		this.modeloTabla.setFilas(listaMails);

		this.modeloTabla.fireTableDataChanged();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

}
