package gui.paneles;

import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

import gui.GuiManager;

public class PanelPrincipal extends JPanel
{

	//ATRIBUTOS
	private JPanel panelBuzones;
	private JPanel panelMails;
	private JPanel panelVisualizacion;
	
	//CONSTRUCTORES
	public PanelPrincipal()
	{
		super();
		this.armarPanel();
	}
	
	//GETTERS
	public JPanel getPanelBuzones() {
		return panelBuzones;
	}
	public void setPanelBuzones(JPanel panelBuzones) {
		this.panelBuzones = panelBuzones;
	}
	public JPanel getPanelMails() {
		return panelMails;
	}
	public void setPanelMails(JPanel panelMails) {
		this.panelMails = panelMails;
	}
	public JPanel getPanelVisualizacion() {
		return panelVisualizacion;
	}
	public void setPanelVisualizacion(JPanel panelVisualizacion) {
		this.panelVisualizacion = panelVisualizacion;
	}
	
	
	//METODOS
	private void armarPanel()
	{
		//Seteo todos los atributos
		this.panelBuzones = new PanelBuzones();
		this.panelMails = new PanelAdministracionMails();
		this.panelVisualizacion = new PanelTrabajo();
			
		//this.cambiarPanelVisualizacionAl((PanelVisualizacionTrabajo)GuiManager.unicaInstancia().getPanelVisualizacionMail());
		
		//Seteo el layout al panel
		FlowLayout layoutDelPanel = new FlowLayout(0,2,2);
		this.setLayout(layoutDelPanel);
		
		//Agrego los botones al panel
		this.add(this.panelBuzones);
		this.add(this.panelMails);
		this.add(this.panelVisualizacion);
	}
	
	//Oculta todos los paneles en la visualizacion
	private void ocultarPanelesVisualizacion()
	{
		//Saco todos los elementos de l
		this.panelVisualizacion.remove(((PanelTrabajo)this.panelVisualizacion).getPanelPrincipalVisualizacion());
	}
	
	//Muestra el panel Mail en la Visualizacion
	public void cambiarPanelVisualizacionAl(PanelVisualizacionTrabajo panelSeleccionado)
	{
		//Oculto todos los paneles
		this.ocultarPanelesVisualizacion();
		
		//Cambia el panel principal al panel seleccionado
		((PanelTrabajo) this.panelVisualizacion).setPanelPrincipalVisualizacion(panelSeleccionado); //LE AGREGUE UN CAST
		
		((PanelTrabajo) this.panelVisualizacion).getPanelPrincipalVisualizacion().actualizarVista(); //LE AGREGUE UN CAST
		
		((PanelTrabajo) this.getPanelVisualizacion()).actualizarVista(); //LE AGREGUE UN CAST

	}
	
	

}