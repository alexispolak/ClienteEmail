package gui.paneles;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultTreeSelectionModel;

import exceptions.DAOException;
import exceptions.LoginException;
import exceptions.MailException;
import exceptions.RecepcionFallidaException;
import gui.GuiManager;
import gui.ImagenesManager;
import modelo.Contacto;
import modelo.ContactosTableModel;

public class PanelBuzones extends JPanel implements ActionListener
{
	//ATRIBUTOS
	
	private JPanel panelBotoneraNuevoMailTraerMail;
	private JPanel panelBotoneraConfiguracionContactos;
	private JPanel panelArbolBuzones;

	
	//GETTERS
	public JPanel getPanelBotoneraNuevoMailTraerMail() {
		return panelBotoneraNuevoMailTraerMail;
	}

	public void setPanelBotoneraNuevoMailTraerMail(PanelBotonera panelBotoneraNuevoMailTraerMail) {
		this.panelBotoneraNuevoMailTraerMail = panelBotoneraNuevoMailTraerMail;
	}
	
	public JPanel getPanelBotoneraConfiguracionContactos() {
		return panelBotoneraConfiguracionContactos;
	}

	public void setPanelBotoneraConfiguracionContactos(PanelBotonera panelBotoneraConfiguracionContactos) {
		this.panelBotoneraConfiguracionContactos = panelBotoneraConfiguracionContactos;
	}
	
	public JPanel getPanelArbolBuzones() {
		return panelArbolBuzones;
	}

	public void setPanelArbolBuzones(JPanel panelArbolBuzones) {
		this.panelArbolBuzones = panelArbolBuzones;
	}

	//CONSTRUCTORES
	public PanelBuzones()
	{
		super();
		this.armarPanel();
	}
	
	//METODOS
	private void armarPanel()
	{
		
		//Seteo todos los atributos
		this.panelBotoneraNuevoMailTraerMail = new PanelBotonera(this.getAccionBotonDescargarMails(this),"", this.getAccionBotonNuevoMail(), "", ImagenesManager.unicaInstancia().getIconoBotonActualizarMails(), ImagenesManager.unicaInstancia().getIconoBotonNuevo());
		this.panelBotoneraConfiguracionContactos = new PanelBotonera(this.getAccionBotonConfiguracion(), "", this.getAccionBotonContactos(), "", ImagenesManager.unicaInstancia().getIconoBotonConfiguracion(), ImagenesManager.unicaInstancia().getIconoBotonContactos());
		this.panelArbolBuzones = new PanelBuzon();
		
		//Establezco el layout
		BorderLayout layoutDelPanel = new BorderLayout();
		this.setLayout(layoutDelPanel);
					
		//Agrego todos los paneles al PabelBuzones.
		this.add(this.panelBotoneraNuevoMailTraerMail,BorderLayout.NORTH);
		this.add(this.panelBotoneraConfiguracionContactos, BorderLayout.SOUTH);
		this.add(this.panelArbolBuzones, BorderLayout.CENTER);
	}
	
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
    public Dimension getMinimumSize() {
        return new Dimension(200, 300);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(200, GuiManager.unicaInstancia().getMarcoAplicacion().getSize().height-50); //50 es el tamanio de la botonera de alto
    }

    @Override
    public void paintComponent(Graphics g) {
        int margin = 10;
        Dimension dim = getSize();
        super.paintComponent(g);
        g.setColor(Color.red);
        g.fillRect(margin, margin, dim.width - margin * 2, dim.height - margin * 2);
    }
    
    private ActionListener getAccionBotonConfiguracion()
    {
    	return GuiManager.unicaInstancia().getAccionMostrarConfiguracion();
    }
    
    private ActionListener getAccionBotonContactos()
    {
    	return GuiManager.unicaInstancia().getAccionMostrarContactos();
    }
    
    private ActionListener getAccionBotonDescargarMails(PanelBuzones modelo)
    {
    	return new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				try
				{
					GuiManager.unicaInstancia().getAdministradorMails().recibir();
					JOptionPane.showMessageDialog(null, "No hay nuevos mails para descargar.");					
				}
				catch(RecepcionFallidaException ref)
				{
					JOptionPane.showMessageDialog(null, ref.getMessage());
				}
				catch(LoginException le)
				{
					JOptionPane.showMessageDialog(null, le.getMessage());
				}
				catch(MailException me)
				{
					JOptionPane.showMessageDialog(null, me.getMessage());
				}
				catch(DAOException daoe)
				{
					JOptionPane.showMessageDialog(null, daoe.getMessage());
				}
			}
		};
    }
    
    private ActionListener getAccionBotonNuevoMail()
    {
    	return GuiManager.unicaInstancia().getAccionMostrarCorreoNuevo();
    }
}
