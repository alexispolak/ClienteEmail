package gui.paneles;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import dao.ConfiguracionPOP3DAO;
import dao.impl.ConfiguracionPop3DaoImpl;
import exceptions.DAOException;
import gui.GuiManager;
import gui.ImagenesManager;
import modelo.ConfiguracionPOP3;
import modelo.ConfiguracionSMTP;
import modelo.Usuario;

public class PanelConfiguracion extends PanelVisualizacionTrabajo implements ActionListener
{
	//ATRIBUTOS
	private final String tituloPop = "POP3";
	private final String tituloSMTP = "SMTP";
	private final String cuenta = "Cuenta";
	private final String usuario = "Usuario";
	private final String password = "Contraseņa";
	
	private JLabel labelTituloPop;
	private JLabel labelTituloSmtp;
	private JLabel labelTituloCuenta;
	private JLabel labelTituloUsuario;
	private JLabel labelTituloPassword;
	
	private JTextField inputUsuario;
	private JTextField inputPassword;
	
	private PanelConfiguracionUsuarioPassword panelConfiguracionUsuarioPassword;
	private PanelConfiguracionHostPortStar panelConfiguracionHostPortStar;
	private PanelConfiguracionConAutorizacion panelConfiguracionConAutorizacion;
	private PanelBotonera panelBotoneraAceptarCancelar;
	
	//CONSTRUCTORES
	public PanelConfiguracion()
	{
		super();

	}
	
	//GETTERS
	public JLabel getLabelTituloPop() {
		return labelTituloPop;
	}
	public void setLabelTituloPop(JLabel labelTituloPop) {
		this.labelTituloPop = labelTituloPop;
	}
	public JLabel getLabelTituloSmtp() {
		return labelTituloSmtp;
	}
	public void setLabelTituloSmtp(JLabel labelTituloSmtp) {
		this.labelTituloSmtp = labelTituloSmtp;
	}
	public JLabel getLabelTituloCuenta() {
		return labelTituloCuenta;
	}
	public void setLabelTituloCuenta(JLabel labelTituloCuenta) {
		this.labelTituloCuenta = labelTituloCuenta;
	}
	public JLabel getLabelTituloUsuario() {
		return labelTituloUsuario;
	}
	public void setLabelTituloUsuario(JLabel labelTituloUsuario) {
		this.labelTituloUsuario = labelTituloUsuario;
	}
	public JLabel getLabelTituloPassword() {
		return labelTituloPassword;
	}
	public void setLabelTituloPassword(JLabel labelTituloPassword) {
		this.labelTituloPassword = labelTituloPassword;
	}
	public JTextField getInputUsuario() {
		return inputUsuario;
	}
	public void setInputUsuario(JTextField inputUsuario) {
		this.inputUsuario = inputUsuario;
	}
	public JTextField getInputPassword() {
		return inputPassword;
	}
	public void setInputPassword(JTextField inputPassword) {
		this.inputPassword = inputPassword;
	}
	public PanelBotonera getPanelBotoneraAceptarCancelar() {
		return panelBotoneraAceptarCancelar;
	}
	public void setPanelBotoneraAceptarCancelar(PanelBotonera panelBotoneraAceptarCancelar) {
		this.panelBotoneraAceptarCancelar = panelBotoneraAceptarCancelar;
	}

	//METODOS
	//Arma el panel de configuracion
	public void armarPanel()
	{	
		String popHost = GuiManager.unicaInstancia().getAdministradorMails().getConfiguracionRecibo().getHost();
		String popPuerto = GuiManager.unicaInstancia().getAdministradorMails().getConfiguracionRecibo().getPuerto();
		String popStarttls = GuiManager.unicaInstancia().getAdministradorMails().getConfiguracionRecibo().getStarttls();
		
		String smtpHost = GuiManager.unicaInstancia().getAdministradorMails().getConfiguracionEnvio().getHost();
		String smtpPuerto = GuiManager.unicaInstancia().getAdministradorMails().getConfiguracionEnvio().getPuerto();
		String smtpStarttls = GuiManager.unicaInstancia().getAdministradorMails().getConfiguracionEnvio().getStarttls();
		String smtpAutoriza = ((ConfiguracionSMTP) GuiManager.unicaInstancia().getAdministradorMails().getConfiguracionEnvio()).getAutorizacion();

		String usuario = GuiManager.unicaInstancia().getAdministradorMails().getUsuario().getNombreUsuario();
		String pass = GuiManager.unicaInstancia().getAdministradorMails().getUsuario().getContrasenia();
		
		this.panelConfiguracionUsuarioPassword = new PanelConfiguracionUsuarioPassword(usuario, pass);
		this.panelConfiguracionHostPortStar = new PanelConfiguracionHostPortStar(popHost, popPuerto, popStarttls);
		this.panelConfiguracionConAutorizacion = new PanelConfiguracionConAutorizacion(smtpHost, smtpPuerto, smtpStarttls, smtpAutoriza);
		
		//Seteo todos los atributos
		this.panelBotoneraAceptarCancelar = new PanelBotonera(this.getAccionSalirConfiguracion(),"",
															  this.getAccionBotonGuardarConfiguracion(this.panelConfiguracionUsuarioPassword, this.panelConfiguracionHostPortStar, this.panelConfiguracionConAutorizacion),"",
															  ImagenesManager.unicaInstancia().getIconoBotonDescartar(), 
															  ImagenesManager.unicaInstancia().getIconoBotonGuardar());
		this.labelTituloPop = new JLabel(this.tituloPop);
		this.labelTituloSmtp = new JLabel(this.tituloSMTP);
		this.labelTituloCuenta = new JLabel(this.cuenta);
		this.labelTituloUsuario = new JLabel(this.usuario);
		this.labelTituloPassword = new JLabel(this.password);
		this.inputUsuario = new JTextField(50);
		this.inputPassword = new JTextField(50);
		
		//Le asigno un color de fondo
		this.setBackground(new Color(0.976f, 0.969f, 0.953f));
		

		//Agrego los botones al panel
		this.add(this.labelTituloCuenta);
		this.add(this.panelConfiguracionUsuarioPassword);
		this.add(this.labelTituloPop);
		this.add(this.panelConfiguracionHostPortStar);
		this.add(this.labelTituloSmtp);
		this.add(this.panelConfiguracionConAutorizacion);
		this.add(this.panelBotoneraAceptarCancelar);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	private ActionListener getAccionBotonGuardarConfiguracion(PanelConfiguracionUsuarioPassword panelUsuarioPass, PanelConfiguracionHostPortStar panelPop, PanelConfiguracionConAutorizacion panelSmtp)
	{
    	return new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				String smtpHost = panelSmtp.getPanelHostPortStar().getInputHost().getText();
				String smtpPuerto = panelSmtp.getPanelHostPortStar().getInputPuerto().getText();
				String smtpStarttls = Boolean.toString(panelSmtp.getPanelHostPortStar().getInputStartls().isSelected());
				String smtpAuth = Boolean.toString(panelSmtp.getInputAutorizacion().isSelected());
				
				String popHost = panelPop.getInputHost().getText();
				String popPuerto = panelPop.getInputPuerto().getText();
				String popStarttls = Boolean.toString(panelPop.getInputStartls().isSelected());
				
				String usuario = panelUsuarioPass.getInputUsuario().getText();
				String pass = panelUsuarioPass.getInputPassword().getText();
				
				try
				{
					//Guardo en la base de datos
					Usuario.actualizarUsuario(usuario, pass);
					ConfiguracionPOP3.actualizarConfiguracionPOP3(popHost, popPuerto, popStarttls);
					ConfiguracionSMTP.actualizarConfiguracionSMTP(smtpHost,smtpPuerto, smtpStarttls, smtpAuth);
					
					JOptionPane.showMessageDialog(null, "Se guardaron los datos con extio.");
				}
				catch (DAOException daoe) 
				{
					daoe.printStackTrace();
		            JOptionPane.showMessageDialog(null, "Se produjo un problema al guardar.");
				}
				finally
				{
					
					//Actualizao el modelo.
					GuiManager.unicaInstancia().getAdministradorMails().getUsuario().setNombreUsuario(usuario);
					GuiManager.unicaInstancia().getAdministradorMails().getUsuario().setContrasenia(pass);

					GuiManager.unicaInstancia().getAdministradorMails().getConfiguracionRecibo().setHost(popHost);
					GuiManager.unicaInstancia().getAdministradorMails().getConfiguracionRecibo().setPuerto(popPuerto);
					GuiManager.unicaInstancia().getAdministradorMails().getConfiguracionRecibo().setStarttls(popStarttls);
					
					GuiManager.unicaInstancia().getAdministradorMails().getConfiguracionEnvio().setHost(smtpHost);
					GuiManager.unicaInstancia().getAdministradorMails().getConfiguracionEnvio().setPuerto(smtpPuerto);
					GuiManager.unicaInstancia().getAdministradorMails().getConfiguracionEnvio().setStarttls(smtpStarttls);
					((ConfiguracionSMTP) GuiManager.unicaInstancia().getAdministradorMails().getConfiguracionEnvio()).setAutorizacion(smtpAuth);
				}
			}
		};
	}
	
	private ActionListener getAccionSalirConfiguracion()
	{
		return GuiManager.unicaInstancia().getAccionMostrarCorreoVacio();
	}

}
