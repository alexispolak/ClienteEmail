package gui.paneles;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import exceptions.MailException;
import modelo.Mail;

public class PanelMail extends PanelVisualizacionTrabajo implements ActionListener
{
	//ATRIBUTOS
	private JPanel panelDeParaAsunto;
	private JPanel panelRedaccion;
	private Mail modelo;
	
	//CONSTRUCTORES
	public PanelMail()
	{
		super();
		this.modelo = null;
	}
	
	//GETTERS
	
	public JPanel getPanelDeParaAsunto() {
		return panelDeParaAsunto;
	}
	public void setPanelDeParaAsunto(JPanel panelDeParaAsunto) {
		this.panelDeParaAsunto = panelDeParaAsunto;
	}
	public JPanel getPanelRedaccion() {
		return panelRedaccion;
	}
	public void setPanelRedaccion(JPanel panelRedaccion) {
		this.panelRedaccion = panelRedaccion;
	}

	public Mail getModelo() {
		return modelo;
	}

	public void setModelo(Mail modelo) {
		this.modelo = modelo;
	}

	//METODOS
	public void armarPanel()
	{
		this.panelDeParaAsunto = new PanelDeParaAsunto();
		this.panelRedaccion = new PanelRedaccion();
		
		//Establezco el layout
		BorderLayout layoutDelPanel = new BorderLayout();
		this.setLayout(layoutDelPanel);

		this.add(this.panelDeParaAsunto, BorderLayout.NORTH);
		this.add(this.panelRedaccion, BorderLayout.CENTER);
	}	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public void nuevoMail(String usuario)
	{
		this.limpiarCampos();
		((PanelDeParaAsunto)this.panelDeParaAsunto).getTextoDe().setText(usuario);

		this.activarCampos();
	}
	
	public void mailRecibido(Mail unMail)
	{
		this.modelo = unMail;
		((PanelDeParaAsunto)this.panelDeParaAsunto).getTextoDe().setText(unMail.getOrigen());
		((PanelDeParaAsunto)this.panelDeParaAsunto).getTextoPara().setText(unMail.getDestinatario());
		((PanelDeParaAsunto)this.panelDeParaAsunto).getTextoAsunto().setText(unMail.getAsunto());
		((PanelRedaccion)this.panelRedaccion).getCampoTexto().setText(unMail.getMensaje());
		this.desactivarCampos();
	}
	
	public void mailRespuesta() throws MailException
	{
		try
		{
			this.activarCampos();
			((PanelDeParaAsunto)this.panelDeParaAsunto).getTextoDe().setText(this.modelo.getDestinatario());
			((PanelDeParaAsunto)this.panelDeParaAsunto).getTextoPara().setText(this.modelo.getOrigen());
			((PanelDeParaAsunto)this.panelDeParaAsunto).getTextoAsunto().setText("Re: "+this.modelo.getAsunto());
			((PanelRedaccion)this.panelRedaccion).getCampoTexto().setText("\n\n------------------------------------------------------------------------------\n"+this.modelo.getMensaje());
		}
		catch(NullPointerException npe)
		{
			throw new MailException("Necesita seleccionar un mail para poder responder", npe);
		}
	}

	public void limpiarCampos() 
	{
		this.modelo = null;
		((PanelDeParaAsunto)this.panelDeParaAsunto).getTextoDe().setText("");
		((PanelDeParaAsunto)this.panelDeParaAsunto).getTextoPara().setText("");
		((PanelDeParaAsunto)this.panelDeParaAsunto).getTextoAsunto().setText("");
		((PanelRedaccion)this.panelRedaccion).getCampoTexto().setText("");	
	}

	private void desactivarCampos()
	{
		((PanelRedaccion)this.panelRedaccion).getCampoTexto().setEditable(false);
		((PanelDeParaAsunto)this.panelDeParaAsunto).getTextoAsunto().setEnabled(false);
		((PanelDeParaAsunto)this.panelDeParaAsunto).getTextoPara().setEnabled(false);
		((PanelDeParaAsunto)this.panelDeParaAsunto).getTextoDe().setEnabled(false);
	}
	
	private void activarCampos()
	{
		((PanelRedaccion)this.panelRedaccion).getCampoTexto().setEditable(true);
		((PanelDeParaAsunto)this.panelDeParaAsunto).getTextoAsunto().setEnabled(true);
		((PanelDeParaAsunto)this.panelDeParaAsunto).getTextoPara().setEnabled(true);
		((PanelDeParaAsunto)this.panelDeParaAsunto).getTextoDe().setEnabled(true);
	}
}
