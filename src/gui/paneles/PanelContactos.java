package gui.paneles;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import dao.ContactoDAO;
import dao.impl.ContactoDaoH2Impl;
import exceptions.DAOException;
import gui.GuiManager;
import gui.ImagenesManager;
import modelo.ConfiguracionPOP3;
import modelo.ConfiguracionSMTP;
import modelo.Contacto;
import modelo.ContactosTableModel;
import modelo.TableCellListener;
import modelo.Usuario;

public class PanelContactos extends PanelVisualizacionTrabajo implements ActionListener
{
	//ATRIBUTOS
	private JTable tablaContactos;
	private ContactosTableModel modeloTabla;
	private JScrollPane panelConScrollParaTabla;
	private PanelBotonera botoneraAgregarEliminarContactos;
	private PanelBotonera botoneraSalir;
	
	//CONSTRUCTORES
	public PanelContactos()
	{
		super();

		this.traerDatos();
	}
	
	//GETTERS
	public JTable getTablaContactos() {
		return tablaContactos;
	}
	public void setTablaContactos(JTable tablaContactos) {
		this.tablaContactos = tablaContactos;
	}
	public ContactosTableModel getModeloTabla() {
		return modeloTabla;
	}
	public void setModeloTabla(ContactosTableModel modeloTabla) {
		this.modeloTabla = modeloTabla;
	}
	public JScrollPane getPanelConScrollParaTabla() {
		return panelConScrollParaTabla;
	}
	public void setPanelConScrollParaTabla(JScrollPane panelConScrollParaTabla) {
		this.panelConScrollParaTabla = panelConScrollParaTabla;
	}
	public PanelBotonera getBotoneraAgregarEliminarContactos() {
		return botoneraAgregarEliminarContactos;
	}
	public void setBotoneraAgregarEliminarContactos(PanelBotonera botoneraAgregarEliminarContactos) {
		this.botoneraAgregarEliminarContactos = botoneraAgregarEliminarContactos;
	}
	public PanelBotonera getBotoneraSalir() {
		return botoneraSalir;
	}
	public void setBotoneraSalir(PanelBotonera botoneraSalir) {
		this.botoneraSalir = botoneraSalir;
	}

	//METODOS
	public void armarPanel()
	{
		//Seteo todos los atributos
		this.modeloTabla = new ContactosTableModel();
		this.tablaContactos = new JTable(this.modeloTabla);
		this.panelConScrollParaTabla = new JScrollPane(this.tablaContactos);
		
		this.botoneraAgregarEliminarContactos = new PanelBotonera(this.getAccionAgregarContacto(this), "", this.getAccionEliminarContacto(this.tablaContactos), "", ImagenesManager.unicaInstancia().getIconoBotonNuevo(), ImagenesManager.unicaInstancia().getIconoBotonBorrar());
		this.botoneraSalir = new PanelBotonera(this.getAccionSalirContactos(), "", null, "", ImagenesManager.unicaInstancia().getIconoBotonDescartar(), "");
		
		TableCellListener tcl = new TableCellListener(this.tablaContactos, new AbstractAction(){
		    public void actionPerformed(ActionEvent e){
		        TableCellListener tcl = (TableCellListener)e.getSource();
		        Contacto contacto = (Contacto) ((ContactosTableModel)tcl.getTable().getModel()).getFilas().get(tcl.getRow());
		        
		        try 
		        {
					Contacto.modificarContacto(contacto);
				} 
		        catch (DAOException daoe) 
		        {	
		        	daoe.printStackTrace();
		        	JOptionPane.showMessageDialog(null, daoe.getMessage());
				}
		    }
		});
		
		JPanel panelAuxiliarBotonera = new JPanel();
		
		//Le asigno un color de fondo
		Color fondo = new Color(0.976f, 0.969f, 0.953f);
		this.setBackground(fondo);
		this.tablaContactos.setBackground(fondo);
		panelAuxiliarBotonera.setBackground(fondo);
		this.panelConScrollParaTabla.setBackground(Color.blue);
		
		//Establezco el layout
		BorderLayout layoutDelPanel = new BorderLayout();
		this.setLayout(layoutDelPanel);
		
		BorderLayout layoutAyuda = new BorderLayout();
		panelAuxiliarBotonera.setLayout(layoutAyuda);
		
		panelAuxiliarBotonera.add(this.botoneraAgregarEliminarContactos, BorderLayout.CENTER);
		panelAuxiliarBotonera.add(this.botoneraSalir, BorderLayout.WEST);
		
		
		this.botoneraSalir.getBotonDerecho().setVisible(false);
		
		//Agrego los componentes
		this.add(panelAuxiliarBotonera, BorderLayout.SOUTH);
		this.add(this.panelConScrollParaTabla, BorderLayout.CENTER);
	}
	
	//Trae los contactos de la base de datos
	private void traerDatos()
	{
		try 
		{
	        ArrayList<Contacto> listaContacto = new ArrayList<Contacto>();
			listaContacto = GuiManager.unicaInstancia().getAdministradorMails().traerContactos();
			
			this.modeloTabla.setFilas(listaContacto);
	
			this.modeloTabla.fireTableDataChanged();
		
		} 
		catch (DAOException daoe) 
		{
			JOptionPane.showMessageDialog(null, daoe.getMessage());
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{		
	}

	
	private ActionListener getAccionAgregarContacto(PanelContactos modelo)
	{
    	return new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
		
				try 
				{
					Contacto.agregarContacto();

					modelo.traerDatos();
				} 
				catch (DAOException daoe) 
				{
					daoe.printStackTrace();
		            JOptionPane.showMessageDialog(null, "Se produjo un problema al agregar el contacto.");
				}
			}
		};
	}
	
	private ActionListener getAccionEliminarContacto(JTable tabla)
	{
    	return new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				try 
				{
					int filaParaBorrar = tabla.getSelectedRow();
					
					if (filaParaBorrar < 0)
						filaParaBorrar = 0;
					
					ArrayList<Contacto> listaContacto = new ArrayList<Contacto>();
					listaContacto = (ArrayList<Contacto>) ((ContactosTableModel)tabla.getModel()).getFilas();
					
					Contacto contactoParaEliminar = listaContacto.get(filaParaBorrar);

					Contacto.borrar(contactoParaEliminar);
					
					GuiManager.unicaInstancia().getAdministradorMails().getListaContactos().remove(contactoParaEliminar);
					
					((ContactosTableModel)tabla.getModel()).setFilas(listaContacto);
					
					((ContactosTableModel)tabla.getModel()).fireTableDataChanged();
				} 
				catch (DAOException daoe) 
				{
					daoe.printStackTrace();
					JOptionPane.showMessageDialog(null, "Se produjo un problema al borrar el contacto.");
				}
				catch (IndexOutOfBoundsException oobe)
				{
					oobe.printStackTrace();
					JOptionPane.showMessageDialog(null, "No hay nada que borrar.");
				}				
			}
		};
	}
	
	private ActionListener getAccionSalirContactos()
	{
		return GuiManager.unicaInstancia().getAccionMostrarCorreoVacio();
	}

}
