package gui.paneles;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import gui.GuiManager;

public class PanelConfiguracionHostPortStar extends JPanel implements ActionListener
{
	//ATRIBUTOS
	private final String textoLabelHost = "Host:                ";
	private final String textoLabelPuerto = "Puerto:            ";
	private final String textoLabelStar = "Starttls:         ";

	private JLabel labelHost;
	private JLabel labelPuerto;
	private JLabel labelStar;
	private JTextField inputHost;
	private JTextField inputPuerto;
	private JCheckBox inputStartls;
	
	//CONSTRUCTORES
	public PanelConfiguracionHostPortStar(String host, String puerto, String starttls)
	{
		super();
		
		this.armarPanel(host, puerto, starttls);
	}
	
	//GETTERS
	public JLabel getLabelHost() {
		return labelHost;
	}
	public void setLabelHost(JLabel labelHost) {
		this.labelHost = labelHost;
	}
	public JLabel getLabelPuerto() {
		return labelPuerto;
	}
	public void setLabelPuerto(JLabel labelPuerto) {
		this.labelPuerto = labelPuerto;
	}
	public JLabel getLabelStar() {
		return labelStar;
	}
	public void setLabelStar(JLabel labelStar) {
		this.labelStar = labelStar;
	}
	public JTextField getInputHost() {
		return inputHost;
	}
	public void setInputHost(JTextField inputHost) {
		this.inputHost = inputHost;
	}
	public JTextField getInputPuerto() {
		return inputPuerto;
	}
	public void setInputPuerto(JTextField inputPuerto) {
		this.inputPuerto = inputPuerto;
	}
	public JCheckBox getInputStartls() {
		return inputStartls;
	}
	public void setInputStartls(JCheckBox inputStartls) {
		this.inputStartls = inputStartls;
	}

	
	//METODOS
	//Arma el panel
	private void armarPanel(String hostText, String puertoText, String starttlsText)
	{
		//Creo paneles auxiliares
		JPanel panelAuxiliarHost = new JPanel();
		JPanel panelAuxiliarPuerto = new JPanel();
		JPanel panelAuxiliarStar = new JPanel();
		
		//Seteo todos los atributos
		this.labelHost = new JLabel(this.textoLabelHost);
		this.labelPuerto = new JLabel(this.textoLabelPuerto);
		this.labelStar = new JLabel(this.textoLabelStar);
		this.inputHost = new JTextField(50);
		this.inputPuerto = new JTextField(50);
		this.inputStartls = new JCheckBox();
		
		this.inputHost.setText(hostText);
		this.inputPuerto.setText(puertoText);
		if(starttlsText != null)
			this.inputStartls.setSelected((starttlsText.equals("true")));
		
		//Seteo el layout de los paneles auxiliares
		panelAuxiliarHost.setLayout(new BorderLayout());
		panelAuxiliarPuerto.setLayout(new BorderLayout());
		panelAuxiliarStar.setLayout(new BorderLayout());
		
		//Agrego los componentes a los paneles auxiliares
		panelAuxiliarHost.add(this.labelHost, BorderLayout.WEST);
		panelAuxiliarHost.add(this.inputHost, BorderLayout.CENTER);

		panelAuxiliarPuerto.add(this.labelPuerto, BorderLayout.WEST);
		panelAuxiliarPuerto.add(this.inputPuerto, BorderLayout.CENTER);

		panelAuxiliarStar.add(this.labelStar, BorderLayout.WEST);
		panelAuxiliarStar.add(this.inputStartls, BorderLayout.CENTER);
		
		//Le asigno un color de fondo
		this.setBackground(new Color(0.976f, 0.969f, 0.953f));
		panelAuxiliarHost.setBackground(new Color(0.976f, 0.969f, 0.953f));
		panelAuxiliarPuerto.setBackground(new Color(0.976f, 0.969f, 0.953f));
		panelAuxiliarStar.setBackground(new Color(0.976f, 0.969f, 0.953f));
		
		//Establezco el layout
		GridLayout layoutDelPanel = new GridLayout(4,1);
		this.setLayout(layoutDelPanel);
		
		//Agrego los componentes al panel
		this.add(panelAuxiliarHost);
		this.add(panelAuxiliarPuerto);
		this.add(panelAuxiliarStar);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
    public Dimension getMinimumSize() {
        return new Dimension(200, 100);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(GuiManager.unicaInstancia().getMarcoAplicacion().getSize().width-675, 100);
    }
    
    @Override
    public void paintComponent(Graphics g) {
        int margin = 10;
        Dimension dim = getSize();
        super.paintComponent(g);
    }	

}
