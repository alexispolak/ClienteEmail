package gui.paneles;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;

import exceptions.DAOException;
import gui.GuiManager;
import gui.ImagenesManager;
import modelo.Contacto;
import modelo.ContactosTableModel;
import modelo.Mail;
import modelo.MailTableModel;

public class PanelAdministracionMails extends JPanel implements ActionListener
{

	//ATRIBUTOS
	private JPanel panelListadoMails;
	private PanelBotonera panelBotoneraEliminarResponder;
	
	//CONSTRUCTORES
	public PanelAdministracionMails()
	{
		super();
		this.armarPanel();
	}
	
	//GETTERS
	public JPanel getPanelListadoMails() {
		return panelListadoMails;
	}

	public void setPanelListadoMails(JPanel panelListadoMails) {
		this.panelListadoMails = panelListadoMails;
	}

	public JPanel getPanelBotoneraEliminarResponder() {
		return panelBotoneraEliminarResponder;
	}

	public void setPanelBotoneraEliminarResponder(PanelBotonera panelBotoneraEliminarResponder) {
		this.panelBotoneraEliminarResponder = panelBotoneraEliminarResponder;
	}
	
	//METODOS
	private void armarPanel()
	{
		//Seteo los atributos
		this.panelListadoMails = new PanelListado();
		this.panelBotoneraEliminarResponder = new PanelBotonera(this.getAccionBotonEliminarMail((PanelListado) this.panelListadoMails), "",this.getAccionBotonResponderMail(),"",ImagenesManager.unicaInstancia().getIconoBotonBorrar(), ImagenesManager.unicaInstancia().getIconoBotonResponder());
		
		//Establezco el layout
		BorderLayout layoutDelPanel = new BorderLayout();
		this.setLayout(layoutDelPanel);
		
		//Agrego los paneles internos al panel de la clase
		this.add(this.panelBotoneraEliminarResponder, BorderLayout.NORTH);
		this.add(this.panelListadoMails, BorderLayout.CENTER);
		
		//Agrego la referencia al GuiManager de los botones
		GuiManager.unicaInstancia().setBotonEliminar(this.panelBotoneraEliminarResponder.getBotonIzquierdo());
		GuiManager.unicaInstancia().setBotonResponder(this.panelBotoneraEliminarResponder.getBotonDerecho());
		
	}

	

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}	

	@Override
    public Dimension getMinimumSize() {
        return new Dimension(450, 200);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(450, GuiManager.unicaInstancia().getMarcoAplicacion().getSize().height-50); //50 es el tamanio de la botonera de alto
    }

    @Override
    public void paintComponent(Graphics g) {
        int margin = 10;
        Dimension dim = getSize();
        super.paintComponent(g);
        g.setColor(Color.red);
        g.fillRect(margin, margin, dim.width - margin * 2, dim.height - margin * 2);
    }	
    
    private ActionListener getAccionBotonEliminarMail(PanelListado panelListado)
	{
    	return new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				JTable tabla = panelListado.getTablaMails();
				try 
				{
					int filaParaBorrar = tabla.getSelectedRow();
					
					if (filaParaBorrar < 0)
						filaParaBorrar = 0;
					
					ArrayList<Mail> listaMail = new ArrayList<Mail>();
					listaMail = (ArrayList<Mail>) ((MailTableModel)tabla.getModel()).getFilas();
					
					Mail mailParaEliminar = listaMail.get(filaParaBorrar);

					Mail.eliminar(mailParaEliminar);
					
					((MailTableModel)tabla.getModel()).setFilas((GuiManager.unicaInstancia().getAdministradorMails().dameMailsConTag(panelListado.getTagActual())));
					
					((MailTableModel)tabla.getModel()).fireTableDataChanged();
				} 
				catch (DAOException daoe) 
				{
					daoe.printStackTrace();
					JOptionPane.showMessageDialog(null, "Se produjo un problema al borrar el contacto.");
				}
				catch (IndexOutOfBoundsException oobe)
				{
					oobe.printStackTrace();
					JOptionPane.showMessageDialog(null, "No hay nada que borrar.");
				}				
			}
		};
	}
    
    private ActionListener getAccionBotonResponderMail() 
    {
		return GuiManager.unicaInstancia().getAccionResponderCorreo();	
	}
}
