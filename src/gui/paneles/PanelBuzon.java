package gui.paneles;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import gui.GuiManager;

public class PanelBuzon extends JPanel implements ActionListener
{

	//ATRIBUTOS
	private final String nombreRoot = "Buzones";
	private final String nombreRecibidos = "Recibidos";
	private final String nombreEnviado = "Enviados";
	private final String nombreEliminado = "Eliminados";
	
	private DefaultMutableTreeNode nodoRoot;
	private JTree arbol;
	private DefaultTreeModel modelo;
	
	//CONSTRUCTORES
	public PanelBuzon()
	{
		super();
		this.armarPanel();
	}
	
	//GETTERS
	public JTree getArbol() {
		return arbol;
	}
	public void setArbol(JTree arbol) {
		this.arbol = arbol;
	}
	public DefaultTreeModel getModelo() {
		return modelo;
	}
	public void setModelo(DefaultTreeModel modelo) {
		this.modelo = modelo;
	}
	public String getNombreRoot() {
		return nombreRoot;
	}
	public DefaultMutableTreeNode getNodoRoot() {
		return nodoRoot;
	}
	public void setNodoRoot(DefaultMutableTreeNode nodoRoot) {
		this.nodoRoot = nodoRoot;
	}
	
	
	//METODOS
	private void armarPanel()
	{
		this.nodoRoot = new DefaultMutableTreeNode (this.nombreRoot);
		this.modelo = new DefaultTreeModel(this.nodoRoot);
		this.modelo.insertNodeInto(new DefaultMutableTreeNode(this.nombreRecibidos), this.nodoRoot, 0);
		this.modelo.insertNodeInto(new DefaultMutableTreeNode(this.nombreEnviado), this.nodoRoot, 1);
		this.modelo.insertNodeInto(new DefaultMutableTreeNode(this.nombreEliminado), this.nodoRoot, 2);
		this.arbol = new JTree(this.modelo);
		
		this.arbol.addMouseListener(new MouseAdapter() 
		{
	        public void mouseClicked(MouseEvent e) 
	        {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode)
                       arbol.getLastSelectedPathComponent();
                if (node == null) return;
                String nodeInfo = (String) node.getUserObject();

                if(nodeInfo.equals("Recibidos"))
                	mostrarRecibidos();
                else if (nodeInfo.equals("Enviados"))
                	mostrarEnviados();
                else if (nodeInfo.equals("Eliminados"))
                	mostrarEliminados();         
	        }
	    });
		
		BorderLayout layoutDelPanel = new BorderLayout();
		this.setLayout(layoutDelPanel);
		
		this.arbol.setBackground(new Color(0.976f, 0.969f, 0.953f));
		this.add(this.arbol, BorderLayout.CENTER);
	}
	
	
	
	@Override
	public void actionPerformed(ActionEvent arg0) 
	{
		// TODO Auto-generated method stub
		
	}

	private void mostrarRecibidos()
	{
		PanelAdministracionMails panelAdmin = (PanelAdministracionMails)GuiManager.unicaInstancia().getPanelPrincipal().getPanelMails();
		PanelListado panelListado = ((PanelListado)panelAdmin.getPanelListadoMails());
		String tag = "Recibido";
		panelListado.establecerValores(GuiManager.unicaInstancia().getAdministradorMails().dameMailsConTag(tag), tag);
	}
	
	private void mostrarEnviados()
	{
		PanelAdministracionMails panelAdmin = (PanelAdministracionMails)GuiManager.unicaInstancia().getPanelPrincipal().getPanelMails();
		PanelListado panelListado = ((PanelListado)panelAdmin.getPanelListadoMails());
		String tag = "Enviado";
		panelListado.establecerValores(GuiManager.unicaInstancia().getAdministradorMails().dameMailsConTag(tag),tag);
	}
	
	private void mostrarEliminados()
	{
		PanelAdministracionMails panelAdmin = (PanelAdministracionMails)GuiManager.unicaInstancia().getPanelPrincipal().getPanelMails();
		PanelListado panelListado = ((PanelListado)panelAdmin.getPanelListadoMails());
		String tag = "Eliminado";
		panelListado.establecerValores(GuiManager.unicaInstancia().getAdministradorMails().dameMailsConTag(tag),tag);
	}

	

	
}
