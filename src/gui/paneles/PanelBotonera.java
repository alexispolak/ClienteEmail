package gui.paneles;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

public class PanelBotonera extends JPanel
{ 
	//ATRIBUTOS
	private JButton botonIzquierdo;
	private JButton botonDerecho;
	
	//GETTERS Y SETTERS
	public JButton getBotonIzquierdo() {
		return botonIzquierdo;
	}

	public void setBotonIzquierdo(JButton botonIzquierdo) {
		this.botonIzquierdo = botonIzquierdo;
	}

	public JButton getBotonDerecho() {
		return botonDerecho;
	}

	public void setBotonDerecho(JButton botonDerecho) {
		this.botonDerecho = botonDerecho;
	}

	
	//CONSTRUCTOR
	public PanelBotonera()
	{
		super();
		System.out.println("Se creo un panel botonera vacio. Hacer uno pasando los actionListener");
		this.armarPanel("BOTON1","BOTON2");
	}
	
	public PanelBotonera(ActionListener accionBotonIzquierdo, String textoBotonIzquierdo, ActionListener accionBotonDerecho, String textoBotonDerecho, String pathIzquierdo, String pathDerecho )
	{
		super();
		this.armarPanel(accionBotonIzquierdo, textoBotonIzquierdo, accionBotonDerecho, textoBotonDerecho, pathIzquierdo, pathDerecho);
	}
	
	//METODOS
	private void armarPanel(String x, String y)
	{
		//Seteo todos los atributos
		this.botonIzquierdo = new JButton(x);
		this.botonDerecho = new JButton(y);
		
		//Como quiero que sea FlowLayout no le asigno ningun layout xq ese es por defecto.
		
		//Agrego los botones al panel
		this.add(this.botonIzquierdo);
		this.add(this.botonDerecho);
	}
	
	private void armarPanel(ActionListener accionBotonIzquierdo, String textoBotonIzquierdo, ActionListener accionBotonDerecho, String textoBotonDerecho, String pathIzquierdo, String pathDerecho)
	{
		//Seteo todos los atributos
		this.botonIzquierdo = new JButton(textoBotonIzquierdo);
		this.botonDerecho = new JButton(textoBotonDerecho);
		
		//Como quiero que sea FlowLayout no le asigno ningun layout xq ese es por defecto.
		
		//Le doy el estilo a los botones
		this.darEstiloABotones(pathIzquierdo, pathDerecho);
		
		//Le asigno un color al container
		this.setBackground(new Color(0.8f, 0.859f, 0.863f));
		
		//Agrego los botones al panel
		this.add(this.botonIzquierdo);
		this.add(this.botonDerecho);
		
		//Asigno los action listeners a los botones
		this.botonIzquierdo.addActionListener(accionBotonIzquierdo);
		this.botonDerecho.addActionListener(accionBotonDerecho);
	}
	
	//Habilitacion y deshabilitacion de botones
	public void habilitarBotonDerecho()
	{
		this.botonDerecho.setEnabled(true);
	}
	public void deshabilitarBotonDerecho()
	{
		this.botonDerecho.setEnabled(false);
	}
	public void habilitarBotonIzquierdo()
	{
		this.botonIzquierdo.setEnabled(true);
	}
	public void deshabilitarBotonIzquierdo()
	{
		this.botonIzquierdo.setEnabled(false);
	}
	
	//Da el color y la imagen del boton
	private void darEstiloABotones(String pathIzquierdo, String pathDerecho)
	{
		//Le asigno los iconos
		this.botonIzquierdo.setIcon(new ImageIcon(pathIzquierdo));
		this.botonDerecho.setIcon(new ImageIcon(pathDerecho));

		//Seteo el fondo plano
		this.botonIzquierdo.setBackground(new Color(0.384f, 0.565f, 0.765f));
		this.botonDerecho.setBackground(new Color(0.384f, 0.565f, 0.765f));
		
		//Seteo el color del texto
		this.botonIzquierdo.setForeground(new Color(0.8f, 0.859f, 0.863f));
		this.botonDerecho.setForeground(new Color(0.8f, 0.859f, 0.863f));
	}
}
