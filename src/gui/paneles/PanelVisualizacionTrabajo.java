package gui.paneles;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

public abstract class PanelVisualizacionTrabajo extends JPanel implements ActionListener
{
	//ATRIBUTOS
	JPanel panelTrabajo;
	
	//CONSTRUCTORES
	public PanelVisualizacionTrabajo() {
		super();
		this.armarPanel();
	}
	
	//GETTERS
	public JPanel getPanelTrabajo() {
		return panelTrabajo;
	}

	public void setPanelTrabajo(JPanel panelTrabajo) {
		this.panelTrabajo = panelTrabajo;
	}

	//METODOS
	public abstract void armarPanel();
	
	public void actualizarVista()
	{
		this.revalidate();
		
		this.repaint();
	}
	
	
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		// TODO Auto-generated method stub
		
	}
}
