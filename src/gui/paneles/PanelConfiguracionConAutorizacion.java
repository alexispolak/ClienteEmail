package gui.paneles;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import gui.GuiManager;

public class PanelConfiguracionConAutorizacion extends JPanel implements ActionListener
{
	//ATRIBUTOS
	private final String textoLabelAutorizacion = "Autorizacion:";
	private JLabel labelAutorizacion;
	private JCheckBox inputAutorizacion;
	
	private PanelConfiguracionHostPortStar panelHostPortStar;
	
	//CONSTRUCTORES
	public PanelConfiguracionConAutorizacion(String host, String puerto, String starttls, String autoriza)
	{
		super();
		
		this.armarPanel(host,puerto,starttls,autoriza);
	}

	//GETTERS
	public JLabel getLabelAutorizacion() {
		return labelAutorizacion;
	}
	public void setLabelAutorizacion(JLabel labelAutorizacion) {
		this.labelAutorizacion = labelAutorizacion;
	}
	public JCheckBox getInputAutorizacion() {
		return inputAutorizacion;
	}
	public void setInputAutorizacion(JCheckBox inputAutorizacion) {
		this.inputAutorizacion = inputAutorizacion;
	}
	public PanelConfiguracionHostPortStar getPanelHostPortStar() {
		return panelHostPortStar;
	}
	public void setPanelHostPortStar(PanelConfiguracionHostPortStar panelHostPortStar) {
		this.panelHostPortStar = panelHostPortStar;
	}

	//METODOS
	private void armarPanel(String hostText, String puertoText, String starttlsText, String autorizaText)
	{
		//Creo un panel auxiliar
		JPanel panelAuxiliarAutorizacion = new JPanel();
		
		//Seteo todos los atributos
		this.panelHostPortStar = new PanelConfiguracionHostPortStar(hostText, puertoText, starttlsText);
		this.labelAutorizacion = new JLabel(this.textoLabelAutorizacion);
		this.inputAutorizacion = new JCheckBox();
		
		if(autorizaText != null)
			this.inputAutorizacion.setSelected((autorizaText.equals("true")));
		
		//Seteo el layout de los paneles auxiliares
		panelAuxiliarAutorizacion.setLayout(new BorderLayout());
		
		//Agrego los componentes a los paneles auxiliares
		panelAuxiliarAutorizacion.add(this.labelAutorizacion, BorderLayout.WEST);
		panelAuxiliarAutorizacion.add(this.inputAutorizacion, BorderLayout.CENTER);
		

		//Le asigno un color de fondo
		this.setBackground(new Color(0.976f, 0.969f, 0.953f));
		panelAuxiliarAutorizacion.setBackground(new Color(0.976f, 0.969f, 0.953f));
		
		//Establezco el layout
		BorderLayout layoutDelPanel = new BorderLayout();
		this.setLayout(layoutDelPanel);
		
		//Agrego el panel auxiliar
		this.panelHostPortStar.add(panelAuxiliarAutorizacion);
		
		//Agrego el componente al panel
		this.add(this.panelHostPortStar, BorderLayout.CENTER);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
    public Dimension getMinimumSize() {
        return new Dimension(200, 100);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(GuiManager.unicaInstancia().getMarcoAplicacion().getSize().width-675, 100);
    }
    
    @Override
    public void paintComponent(Graphics g) {
        int margin = 10;
        Dimension dim = getSize();
        super.paintComponent(g);
        g.setColor(Color.red);
        g.fillRect(margin, margin, dim.width - margin * 2, dim.height - margin * 2);
    }	
}
