package gui.paneles;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import gui.GuiManager;

public class PanelConfiguracionUsuarioPassword extends JPanel implements ActionListener
{
	//ATRIBUTOS
	private final String textoLabelUsuario = "Usuario:        ";
	private final String textoLabelPassword = "Contraseņa: ";
	private JLabel labelUsuario;
	private JLabel labelPassword;
	private JTextField inputUsuario;
	private JTextField inputPassword;
	
	//CONSTRUCTORES
	public PanelConfiguracionUsuarioPassword(String usuario, String pass)
	{
		super();
		
		this.armarPanel(usuario, pass);
	}

	//GETTERS	
	public JLabel getLabelUsuario() {
		return labelUsuario;
	}
	public void setLabelUsuario(JLabel labelUsuario) {
		this.labelUsuario = labelUsuario;
	}
	public JLabel getLabelPassword() {
		return labelPassword;
	}
	public void setLabelPassword(JLabel labelPassword) {
		this.labelPassword = labelPassword;
	}
	public JTextField getInputUsuario() {
		return inputUsuario;
	}
	public void setInputUsuario(JTextField inputUsuario) {
		this.inputUsuario = inputUsuario;
	}
	public JTextField getInputPassword() {
		return inputPassword;
	}
	public void setInputPassword(JTextField inputPassword) {
		this.inputPassword = inputPassword;
	}
	
	//METODOS
	private void armarPanel(String usuarioText, String passText)
	{
		//Creo un panel auxiliar
		JPanel panelAuxiliarUsuario = new JPanel();
		JPanel panelAuxiliarPassword = new JPanel();
		
		//Seteo todos los atributos
		this.labelUsuario = new JLabel(this.textoLabelUsuario);
		this.labelPassword = new JLabel(this.textoLabelPassword);
		this.inputUsuario = new JTextField();
		this.inputPassword = new JTextField();
		
		this.inputUsuario.setText(usuarioText);
		this.inputPassword.setText(passText);
		
		//Seteo el layout de los paneles auxiliares
		panelAuxiliarUsuario.setLayout(new BorderLayout());
		panelAuxiliarPassword.setLayout(new BorderLayout());
		
		//Agrego los componentes a los paneles auxiliares
		panelAuxiliarUsuario.add(this.labelUsuario, BorderLayout.WEST);
		panelAuxiliarUsuario.add(this.inputUsuario, BorderLayout.CENTER);
		
		panelAuxiliarPassword.add(this.labelPassword, BorderLayout.WEST);
		panelAuxiliarPassword.add(this.inputPassword, BorderLayout.CENTER);
		
		//Le asigno un color de fondo
		this.setBackground(new Color(0.976f, 0.969f, 0.953f));
		panelAuxiliarUsuario.setBackground(new Color(0.976f, 0.969f, 0.953f));
		panelAuxiliarPassword.setBackground(new Color(0.976f, 0.969f, 0.953f));
		
		//Establezco el layout
		GridLayout layoutDelPanel = new GridLayout(2,1);
		this.setLayout(layoutDelPanel);
		
		//Agrego los componentes al panel
		this.add(panelAuxiliarUsuario);
		this.add(panelAuxiliarPassword);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
    public Dimension getMinimumSize() {
        return new Dimension(200, 50);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(GuiManager.unicaInstancia().getMarcoAplicacion().getSize().width-675, 50);
    }
    
    @Override
    public void paintComponent(Graphics g) {
        int margin = 10;
        Dimension dim = getSize();
        super.paintComponent(g);
        g.setColor(Color.red);
        g.fillRect(margin, margin, dim.width - margin * 2, dim.height - margin * 2);
    }	

}
