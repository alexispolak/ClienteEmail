package gui.paneles;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import gui.GuiManager;

public class PanelDeParaAsunto extends JPanel implements ActionListener
{
	
	//ATRIBUTOS
	private final String textoLabelDe = "De:         ";
	private final String textoLabelPara = "Para:     ";
	private final String textoLabelAsunto = "Asunto: ";
	private JLabel labelDe;
	private JLabel labelPara;
	private JLabel labelAsunto;
	private JTextField textoDe;
	private JTextField textoPara;
	private JTextField textoAsunto;
	
	//CONSTRUCTORES
	public PanelDeParaAsunto()
	{
		super();
		this.armarPanel(true, "");
	}
	
	public PanelDeParaAsunto(boolean puedoModificar, String textoDe)
	{
		super();
		this.armarPanel(puedoModificar, textoDe);
	}
	
	//GETTERS
	public JTextField getTextoDe() {
		return textoDe;
	}
	public void setTextoDe(JTextField textoDe) {
		this.textoDe = textoDe;
	}
	public JTextField getTextoPara() {
		return textoPara;
	}
	public void setTextoPara(JTextField textoPara) {
		this.textoPara = textoPara;
	}
	public JTextField getTextoAsunto() {
		return textoAsunto;
	}
	public void setTextoAsunto(JTextField textoAsunto) {
		this.textoAsunto = textoAsunto;
	}
	public JLabel getLabelDe() {
		return labelDe;
	}
	public void setLabelDe(JLabel labelDe) {
		this.labelDe = labelDe;
	}
	public JLabel getLabelPara() {
		return labelPara;
	}
	public void setLabelPara(JLabel labelPara) {
		this.labelPara = labelPara;
	}
	public JLabel getLabelAsunto() {
		return labelAsunto;
	}

	public void setLabelAsunto(JLabel labelAsunto) {
		this.labelAsunto = labelAsunto;
	}

	//METODOS
	public void armarPanel(boolean puedoModificar, String textoDe)
	{
		JPanel panelAuxiliarDe = new JPanel();
		JPanel panelAuxiliarPara = new JPanel();
		JPanel panelAuxiliarAsunto = new JPanel();
		
		this.labelDe = new JLabel(this.textoLabelDe);
		this.labelPara = new JLabel(this.textoLabelPara);
		this.labelAsunto = new JLabel(this.textoLabelAsunto);
		
		this.textoDe = new JTextField();
		this.textoPara = new JTextField();
		this.textoAsunto = new JTextField();
		
		this.textoDe.setText(textoDe);
		this.textoDe.setEnabled(false);
		this.textoDe.setDisabledTextColor(Color.black);
		this.textoPara.setDisabledTextColor(Color.black);
		this.textoAsunto.setDisabledTextColor(Color.black);
		
		if(!puedoModificar)
		{
			this.textoPara.setEnabled(false);
			this.textoAsunto.setEnabled(false);
			this.textoPara.setDisabledTextColor(Color.black);
			this.textoAsunto.setDisabledTextColor(Color.black);
		}

		panelAuxiliarDe.setLayout(new BorderLayout());
		panelAuxiliarPara.setLayout(new BorderLayout());
		panelAuxiliarAsunto.setLayout(new BorderLayout());
		
		panelAuxiliarDe.add(this.labelDe, BorderLayout.WEST);
		panelAuxiliarDe.add(this.textoDe, BorderLayout.CENTER);

		panelAuxiliarPara.add(this.labelPara, BorderLayout.WEST);
		panelAuxiliarPara.add(this.textoPara, BorderLayout.CENTER);
		
		panelAuxiliarAsunto.add(this.labelAsunto, BorderLayout.WEST);
		panelAuxiliarAsunto.add(this.textoAsunto, BorderLayout.CENTER);
		
		GridLayout layoutDelPanel = new GridLayout(3,1);
		this.setLayout(layoutDelPanel);

		this.add(panelAuxiliarDe);
		this.add(panelAuxiliarPara);
		this.add(panelAuxiliarAsunto);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
		
}
