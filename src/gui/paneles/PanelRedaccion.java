package gui.paneles;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class PanelRedaccion extends JPanel
{
	//ATRIBUTOS
	private JTextArea campoTexto;
	
	//CONSTRUCTORES
	public PanelRedaccion()
	{
		super();
		this.armarPanel(true, "");
	}
	
	public PanelRedaccion(boolean puedoModificar, String textoMail)
	{
		super();
		this.armarPanel(puedoModificar, textoMail);
	}
	
	
	
	//GETTERS
	
	public JTextArea getCampoTexto() {
		return campoTexto;
	}

	public void setCampoTexto(JTextArea campoTexto) {
		this.campoTexto = campoTexto;
	}

	//METODOS
	private void armarPanel(boolean puedoModificar, String textoMail)
	{
		this.campoTexto = new JTextArea();
		this.campoTexto.setText(textoMail);
		
		if(!puedoModificar)
		{
			this.campoTexto.setEditable(false);
			this.campoTexto.setDisabledTextColor(Color.black);
		}
		
		BorderLayout layoutDelPanel = new BorderLayout();
		this.setLayout(layoutDelPanel);
		
		this.campoTexto.setBackground(new Color(0.976f, 0.969f, 0.953f));
		this.add(this.campoTexto, BorderLayout.CENTER);
	}

}
