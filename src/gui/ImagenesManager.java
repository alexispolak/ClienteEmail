package gui;

public class ImagenesManager 
{
	
	public static ImagenesManager unicaInstancia;
	
	private ImagenesManager(){}
	
	public static ImagenesManager unicaInstancia()
	{
		if(unicaInstancia == null)
		{
			unicaInstancia = new ImagenesManager();
		}
		
		return unicaInstancia;
	}
	private final String pathParaCarpetaImagenes = "C:/Users/alexi/Documents/Proyectos/Java Workspace/ClienteCorreo/src/gui/paneles/imagenes/";
	
	private final String iconoBotonActualizarMails = "actualizar.png";
	private final String iconoBotonConfiguracion = "configuracion.png";
	private final String iconoBotonContactos = "contactos.png";
	private final String iconoBotonBorrarMail = "eliminar.png";
	private final String iconoBotonMailLeido = "";
	private final String iconoBotonMailNoLeido = "";
	private final String iconoBotonNuevoMail = "nuevo.png";
	private final String iconoBotonResponder = "responder.png";
	private final String iconoBotonEnviar = "enviar.png";
	private final String iconoBotonDescartar = "descartar.png";
	private final String iconoBotonGuardar = "guardar.png";

	public String getIconoBotonActualizarMails() {
		return this.pathParaCarpetaImagenes+iconoBotonActualizarMails;
	}

	public String getIconoBotonConfiguracion() {
		return this.pathParaCarpetaImagenes+iconoBotonConfiguracion;
	}

	public String getIconoBotonContactos() {
		return this.pathParaCarpetaImagenes+iconoBotonContactos;
	}

	public String getIconoBotonBorrar() {
		return this.pathParaCarpetaImagenes+iconoBotonBorrarMail;
	}

	public String getIconoBotonMailLeido() {
		return this.pathParaCarpetaImagenes+iconoBotonMailLeido;
	}

	public String getIconoBotonMailNoLeido() {
		return this.pathParaCarpetaImagenes+iconoBotonMailNoLeido;
	}

	public String getIconoBotonNuevo() {
		return this.pathParaCarpetaImagenes+iconoBotonNuevoMail;
	}

	public String getIconoBotonResponder() {
		return this.pathParaCarpetaImagenes+iconoBotonResponder;
	}

	public String getIconoBotonEnviar() {
		return this.pathParaCarpetaImagenes+iconoBotonEnviar;
	}

	public String getIconoBotonDescartar() {
		return this.pathParaCarpetaImagenes+iconoBotonDescartar;
	}

	public String getIconoBotonGuardar() {
		return this.pathParaCarpetaImagenes+iconoBotonGuardar;
	}
	
	
	
	
}
