package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import exceptions.MailException;
import gui.paneles.PanelConfiguracion;
import gui.paneles.PanelContactos;
import gui.paneles.PanelMail;
import gui.paneles.PanelPrincipal;
import gui.paneles.PanelTrabajo;
import gui.paneles.PanelVisualizacionTrabajo;
import modelo.AdministradorMails;
import modelo.Mail;

public class GuiManager 
{
	//Singleton
	private static GuiManager guiManagerUnicaInstancia;
	
	//Constructor privado para que no se pueda instanciar
	private GuiManager(){}
	
	//Metodo que devuelve la unica instancia
	public static GuiManager unicaInstancia()
	{
		if(guiManagerUnicaInstancia == null)
		{
			guiManagerUnicaInstancia = new GuiManager();
		}
		
		return guiManagerUnicaInstancia;	
	}
	
	//ATRIBUTOS
	private JFrame marcoAplicacion;
	private PanelPrincipal panelPrincipal;
	
	//Paneles intercambiables.
	private PanelVisualizacionTrabajo panelVisualizacionMail;
	private PanelVisualizacionTrabajo panelVisualizacionContactos;
	private PanelVisualizacionTrabajo panelVisualizacionConfiguracion;
	
	//Botones de la aplicacion que pueden desactivarse
	private JButton botonEliminar;
	private JButton botonResponder;
	private JButton botonEnviar;
	private JButton botonDescartar;
	
	//Modelo Mails
	private AdministradorMails administradorMails;
	
		
	//METODOS	
	public void abrirAplicacion()
	{
		this.inicializarPanelesVisualizacion();
		
		this.marcoAplicacion = new JFrame("Cliente Correo");
		
		this.panelPrincipal = new PanelPrincipal();
		
		this.marcoAplicacion.getContentPane().add(this.panelPrincipal);

		this.marcoAplicacion.setMinimumSize(new Dimension(1200, 800));
		this.marcoAplicacion.setLocationRelativeTo(null);
		this.marcoAplicacion.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.marcoAplicacion.setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.marcoAplicacion.setVisible(true);
	}

	//Crea los paneles de visualizacion para despues poder intercambiarlos como quiera
	private void inicializarPanelesVisualizacion() 
	{
		this.panelVisualizacionMail = new PanelMail();
		this.panelVisualizacionConfiguracion = new PanelConfiguracion();
		this.panelVisualizacionContactos = new PanelContactos();
	}
	
	//Muestra el panel que recibe por parametro
	public void cambiarPanelVisualizacion(PanelVisualizacionTrabajo panelSeleccionado)
	{
		this.panelPrincipal.cambiarPanelVisualizacionAl(panelSeleccionado);
		
		//Re acomodo los elementos y repinto la pantalla
		this.panelPrincipal.getPanelVisualizacion().revalidate();
		this.panelPrincipal.getPanelVisualizacion().repaint();
	}

	//GETTERS Y SETTERS
	public static GuiManager getGuiManagerUnicaInstancia() {
		return guiManagerUnicaInstancia;
	}
	public static void setGuiManagerUnicaInstancia(GuiManager guiManagerUnicaInstancia) {
		GuiManager.guiManagerUnicaInstancia = guiManagerUnicaInstancia;
	}
	public JFrame getMarcoAplicacion() {
		return marcoAplicacion;
	}
	public void setMarcoAplicacion(JFrame marcoAplicacion) {
		this.marcoAplicacion = marcoAplicacion;
	}
	public PanelPrincipal getPanelPrincipal() {
		return panelPrincipal;
	}
	public void setPanelPrincipal(PanelPrincipal panelPrincipal) {
		this.panelPrincipal = panelPrincipal;
	}
	public JButton getBotonEliminar() {
		return botonEliminar;
	}
	public void setBotonEliminar(JButton botonEliminar) {
		this.botonEliminar = botonEliminar;
	}
	public JButton getBotonResponder() {
		return botonResponder;
	}
	public void setBotonResponder(JButton botonResponder) {
		this.botonResponder = botonResponder;
	}
	public JButton getBotonEnviar() {
		return botonEnviar;
	}
	public void setBotonEnviar(JButton botonEnviar) {
		this.botonEnviar = botonEnviar;
	}
	public JButton getBotonDescartar() {
		return botonDescartar;
	}
	public void setBotonDescartar(JButton botonDescartar) {
		this.botonDescartar = botonDescartar;
	}
	public JPanel getPanelVisualizacionMail() {
		return panelVisualizacionMail;
	}
	public void setPanelVisualizacionMail(PanelVisualizacionTrabajo panelVisualizacionMail) {
		this.panelVisualizacionMail = panelVisualizacionMail;
	}
	public JPanel getPanelVisualizacionContactos() {
		return panelVisualizacionContactos;
	}
	public void setPanelVisualizacionContactos(PanelVisualizacionTrabajo panelVisualizacionContactos) {
		this.panelVisualizacionContactos = panelVisualizacionContactos;
	}
	public JPanel getPanelVisualizacionConfiguracion() {
		return panelVisualizacionConfiguracion;
	}
	public void setPanelVisualizacionConfiguracion(PanelVisualizacionTrabajo panelVisualizacionConfiguracion) {
		this.panelVisualizacionConfiguracion = panelVisualizacionConfiguracion;
	}
	public AdministradorMails getAdministradorMails() {
		return administradorMails;
	}

	public void setAdministradorMails(AdministradorMails administradorMails) {
		this.administradorMails = administradorMails;
	}

	
	
	/* ACCIONES */
	public ActionListener getAccionMostrarContactos() 
	{
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Accion Mostrar Contactos");
				((PanelTrabajo) GuiManager.unicaInstancia().getPanelPrincipal().getPanelVisualizacion()).getPanelBotoneraSuperior().setVisible(false);
				GuiManager.unicaInstancia().cambiarPanelVisualizacion((PanelVisualizacionTrabajo) GuiManager.getGuiManagerUnicaInstancia().getPanelVisualizacionContactos());
			}
		};
	}

	public ActionListener getAccionMostrarConfiguracion() 
	{
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Accion Mostrar Configuracion");
				((PanelTrabajo) GuiManager.unicaInstancia().getPanelPrincipal().getPanelVisualizacion()).getPanelBotoneraSuperior().setVisible(false);
				GuiManager.unicaInstancia().cambiarPanelVisualizacion((PanelVisualizacionTrabajo) GuiManager.getGuiManagerUnicaInstancia().getPanelVisualizacionConfiguracion());
			}
		};
	}
	
	public ActionListener getAccionMostrarCorreoVacio() 
	{
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Accion Mostrar Correo Vacio");
				((PanelTrabajo) GuiManager.unicaInstancia().getPanelPrincipal().getPanelVisualizacion()).getPanelBotoneraSuperior().setVisible(true);
				GuiManager.unicaInstancia().cambiarPanelVisualizacion((PanelVisualizacionTrabajo) GuiManager.getGuiManagerUnicaInstancia().getPanelVisualizacionMail());
			}
		};
	}
	
	public ActionListener getAccionMostrarCorreoNuevo() 
	{
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Accion Mostrar Correo Nuevo");
				((PanelTrabajo) GuiManager.unicaInstancia().getPanelPrincipal().getPanelVisualizacion()).getPanelBotoneraSuperior().setVisible(true);
				PanelVisualizacionTrabajo panelVisualizacionMail = (PanelVisualizacionTrabajo) GuiManager.getGuiManagerUnicaInstancia().getPanelVisualizacionMail();
				((PanelMail)panelVisualizacionMail).nuevoMail(GuiManager.unicaInstancia().getAdministradorMails().getUsuario().getNombreUsuario());
				GuiManager.unicaInstancia().cambiarPanelVisualizacion(panelVisualizacionMail);
			}
		};
	}
		
	public void getAccionMostrarCorreoRecibido(Mail unMail) 
	{
		((PanelTrabajo) GuiManager.unicaInstancia().getPanelPrincipal().getPanelVisualizacion()).getPanelBotoneraSuperior().setVisible(true);
		PanelVisualizacionTrabajo panelVisualizacionMail = (PanelVisualizacionTrabajo) GuiManager.getGuiManagerUnicaInstancia().getPanelVisualizacionMail();
		((PanelMail)panelVisualizacionMail).mailRecibido(unMail);
		GuiManager.unicaInstancia().cambiarPanelVisualizacion(panelVisualizacionMail);
	}
	
	public ActionListener getAccionResponderCorreo() 
	{
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try 
				{
					System.out.println("Accion Responder Correo ");
					((PanelTrabajo) GuiManager.unicaInstancia().getPanelPrincipal().getPanelVisualizacion()).getPanelBotoneraSuperior().setVisible(true);
					PanelVisualizacionTrabajo panelVisualizacionMail = (PanelVisualizacionTrabajo) GuiManager.getGuiManagerUnicaInstancia().getPanelVisualizacionMail();
					((PanelMail)panelVisualizacionMail).mailRespuesta();
					GuiManager.unicaInstancia().cambiarPanelVisualizacion(panelVisualizacionMail);
				} 
				catch (MailException me) 
				{
					me.printStackTrace();
					JOptionPane.showMessageDialog(null, me.getMessage());
				}
			}
		};
	}

	public ActionListener getAccionBorrarCamposMail()
	{
		return new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				System.out.println("Accion Borrar Campos Mail");
				PanelVisualizacionTrabajo panelVisualizacionMail = (PanelVisualizacionTrabajo) GuiManager.getGuiManagerUnicaInstancia().getPanelVisualizacionMail();
				((PanelMail)panelVisualizacionMail).limpiarCampos();
			}
		};
	}
	
	
}
