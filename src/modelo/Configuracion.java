package modelo;

import exceptions.DAOException;

public abstract class Configuracion 
{
	//ATRIBUTOS
	protected String host;
	protected String puerto;
	protected String starttls;
	
	//CONSTRUCTORES
	public Configuracion() throws DAOException
	{
	}
	
	public Configuracion(String host, String puerto, String starttls)
	{
		this.host = host;
		this.puerto = puerto;
		this.starttls = starttls;
	}
	
	//GETTERS
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getPuerto() {
		return puerto;
	}
	public void setPuerto(String puerto) {
		this.puerto = puerto;
	}
	public String getStarttls() {
		return starttls;
	}
	public void setStarttls(String starttls) {
		this.starttls = starttls;
	}
	
	//METODOS
	public abstract void leerConfiguracion() throws DAOException ;

	
}
