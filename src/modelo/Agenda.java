package modelo;

import java.util.ArrayList;

public class Agenda 
{
	//ATRIBUTOS
	private ArrayList<Contacto> listaContactos;
	
	//CONSTRUCTORES
	public Agenda()
	{
		System.out.println("Esta agenda esta vacia");
		this.listaContactos = new ArrayList<Contacto>();
	}

	public Agenda(ArrayList<Contacto> listaContactos)
	{
		this.listaContactos = listaContactos;
	}
	
	//GETTERS
	public ArrayList<Contacto> getListaContactos() {
		return listaContactos;
	}
	public void setListaContactos(ArrayList<Contacto> listaContactos) {
		this.listaContactos = listaContactos;
	}
	
	//METODOS
	public String toString()
	{
		String cantidad = "Cantidad Contatos: "+this.listaContactos.size()+"\n";
		String contactos = "";
		for (Contacto unContacto : this.listaContactos) {
			contactos += unContacto.toString()+"\n";
		}
		String fin = "--------Fin de Lista Contactos--------";
		return cantidad+contactos+fin;
	}

	
}
