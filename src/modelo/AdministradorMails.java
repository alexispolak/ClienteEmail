package modelo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.AuthenticationFailedException;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.FolderNotFoundException;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.JOptionPane;

import org.jsoup.Jsoup;

import com.sun.mail.smtp.SMTPAddressFailedException;

import dao.ContactoDAO;
import dao.MailDAO;
import dao.impl.ContactoDaoH2Impl;
import dao.impl.MailDaoH2Impl;
import exceptions.DAOException;
import exceptions.EnvioFallidoException;
import exceptions.LoginException;
import exceptions.MailException;
import exceptions.RecepcionFallidaException;

public class AdministradorMails 
{
	//TODO preguntar si puedo clavar un singleton.
	//TODO ver como hacer un enum con esto.
	public String tagBorrado = "borrado";
	public String tagLido = "leido";
	public String tagNuevo = "nuevo";
	public String tagEnviado = "enviado";
	public String tagImportante = "importante";
	
	//ATRIBUTOS
	private Configuracion configuracionEnvio;
	private Configuracion configuracionRecibo;
	private ArrayList<Message> listaInboxRecibidos;
	private ArrayList<Mail> listaMails;
	private Usuario usuario;
	private ArrayList<Contacto> listaContactos;
	private MailDAO dao;
	
	//CONSTRUCTORES
	public AdministradorMails() throws DAOException
	{
		this.listaMails = new ArrayList<Mail>();
		this.listaContactos = new ArrayList<Contacto>();
		System.out.println("Se cargo el adminitrador de mails con mi configuracion por defecto");
		this.configuracionEnvio = new ConfiguracionSMTP("smtp.gmail.com", "25", "true", "true");
		this.configuracionRecibo = new ConfiguracionPOP3("pop.gmail.com", "995", "true");
		this.leerMailsBaseDeDatos();
	}
	
	public AdministradorMails(Configuracion envio, Configuracion recibo) throws DAOException
	{
		this.listaMails = new ArrayList<Mail>();
		this.listaContactos = new ArrayList<Contacto>();
		this.configuracionEnvio = envio;
		this.configuracionRecibo = recibo;
		System.out.println("CONFIGURACION: \n Envio:"+this.configuracionEnvio.toString()+"\nRecibo: "+this.configuracionRecibo.toString()+"\n FIN CONFIGIRACION \n");
		this.leerMailsBaseDeDatos();	
	}
	
	

	//GETTERS
	public Configuracion getConfiguracionEnvio() {
		return configuracionEnvio;
	}
	public void setConfiguracionEnvio(Configuracion configuracionEnvio) {
		this.configuracionEnvio = configuracionEnvio;
	}
	public Configuracion getConfiguracionRecibo() {
		return configuracionRecibo;
	}
	public void setConfiguracionRecibo(Configuracion configuracionRecibo) {
		this.configuracionRecibo = configuracionRecibo;
	}
	public ArrayList<Message> getListaInboxRecibidos() {
		return listaInboxRecibidos;
	}
	public void setListaInboxRecibidos(ArrayList<Message> listaMailsInbox) {
		this.listaInboxRecibidos = listaMailsInbox;
	}
	public ArrayList<Mail> getListaMails() {
		return listaMails;
	}
	public void setListaMails(ArrayList<Mail> listaMails) {
		this.listaMails = listaMails;
	}
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public ArrayList<Contacto> getListaContactos() {
		return listaContactos;
	}

	public void setListaContactos(ArrayList<Contacto> listaContactos) {
		this.listaContactos = listaContactos;
	}
	
	//METODOS
	

	//CREACION DE SESION
	//Crea una sesion para el mail. Autentifica que el usuario y contraseņa esten bien
	private static Session crearSesion(Properties propiedades, String usuario, String password)
	{
		return Session.getInstance(propiedades, new Authenticator()
		{
			protected javax.mail.PasswordAuthentication getPasswordAuthentication()
			{
				System.out.println("POR AUTENTIFICAR:\nUsuario: "+usuario+"\nContraseņa: "+password+"\nFIN AUTENTIFICACION \n");
				return new javax.mail.PasswordAuthentication(usuario, password);
			}
		});
	}

	//CONFIGURACION
	
	///Configura las propiedades del correo que se va a enviar
	private Properties configuracionEnvioMail()  throws NullPointerException
	{
		Properties propiedades = new Properties();
		
		propiedades.put("mail.smtp.host", this.configuracionEnvio.getHost());
		propiedades.put("mail.smtp.auth", ((ConfiguracionSMTP) this.configuracionEnvio).getAutorizacion());
		propiedades.put("mail.smtp.starttls.enable",this.configuracionEnvio.getStarttls());
		propiedades.put("mail.smtp.port", this.configuracionEnvio.getPuerto());
		
		return propiedades;
	}
			
		
	///Configura las propiedades del correo que se va a recibir
	private Properties configuracionReciboMail() throws NullPointerException
	{
		Properties propiedades = new Properties();

		propiedades.put("mail.pop3s.host", this.configuracionRecibo.getHost());
		propiedades.put("mail.pop3s.port", this.configuracionRecibo.getPuerto());
		propiedades.put("mail.pop3s.starttls.enable", this.configuracionRecibo.getStarttls());

		return propiedades;
	}
	
	
	//ENVIO DE CORREO
	
	//Se encarga del envio de correos
	public void enviar(Mail mail) throws LoginException, EnvioFallidoException, DAOException
	{
		try
		{
			Properties propiedades = this.configuracionEnvioMail();
			
			Session unaSesion = this.crearSesion(propiedades, this.usuario.getNombreUsuario(), this.usuario.getContrasenia());
			
			Message mensaje = new MimeMessage(unaSesion);
			
			//Seteo el origne del email
			mensaje.setFrom(new InternetAddress(mail.getOrigen()));
			
			//Seteo el destinatario
			mensaje.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail.getDestinatario()));
			
			//Seteo el Asunto
			mensaje.setSubject(mail.getAsunto());
			
			//Seteo el mensaje		
			mensaje.setText(mail.getMensaje());
			
			if(this.chequeoOrigenMail(mail))
			{
				//Envio el mensaje
				Transport.send(mensaje);
				
				System.out.println("Se envio el correo de forma exitosa");

				this.marcarCorreoComo(this.tagEnviado, mail);		
			}
			else
				throw new EnvioFallidoException("No se puede mandar mail como si fueras otro.\nPresiona + para nu nuevo correo.\nPresiona <-- para responder uno seleccionado.");
		}
		
		catch(SMTPAddressFailedException smtpafe)
		{
			smtpafe.printStackTrace();
			throw new EnvioFallidoException(smtpafe.getMessage());
		}
		catch(SendFailedException sfe)
		{
			sfe.printStackTrace();
			throw new EnvioFallidoException(sfe.getMessage());
		}
		catch(AuthenticationFailedException afe)
		{
			  afe.printStackTrace();
			  throw new LoginException("Error en el logueo."); 
		}
		catch(AddressException adde)
		{
			adde.printStackTrace();
			throw new EnvioFallidoException("Error en la direccion de correo.\n"+adde.getMessage());
		}
		catch(MessagingException me)
		{
			me.printStackTrace();
			throw new EnvioFallidoException("Error en creacion de mensaje.\n"+me.getMessage());
		}
		catch(NullPointerException npe)
		{
			throw new EnvioFallidoException("Error en la configuracion del correo. (?");
		}
		catch(EnvioFallidoException efe)
		{
			throw new EnvioFallidoException(efe.getMessage());
		}
		catch(DAOException daoe)
		{
			throw(daoe);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw new EnvioFallidoException("Error no esperado al enviar un correo");
		}		
	}

	
	private boolean chequeoOrigenMail(Mail mail) 
	{
		return mail.getOrigen().toLowerCase().equals(this.usuario.getNombreUsuario().toLowerCase());
	}

	//RECIBIR CORREO
	
	//Recibe los mails de una casilla
	public void recibir() throws RecepcionFallidaException, LoginException, MailException, DAOException
	{
		Properties propiedades = null;
		try
		{
			propiedades = this.configuracionReciboMail();
			
			System.out.println("PROPIEDADES SESION \nHost: "+this.configuracionRecibo.getHost()+"\nPuerto"+this.configuracionRecibo.getPuerto()+ "\nStarttls: "+this.configuracionRecibo.getStarttls()+"\nFIN PROPIEDADES SESION\n");
			System.out.println("DATOS SESION \nUsuario: "+this.usuario.getNombreUsuario()+"\nContraseņa: "+this.usuario.getContrasenia()+"\n FIN DATOS SESION\n");
		}
		catch(NullPointerException npe)
		{
			throw new RecepcionFallidaException("Error en la configuracion del correo.");
		}
		
		try
		{
			Session unaSesion = crearSesion(propiedades, this.usuario.getNombreUsuario(), this.usuario.getContrasenia());
			
			//Crea un almacen de POP3
			Store almacenamientoMails = unaSesion.getStore("pop3s"); //store.getDefaultDoldeR().list("*");
			
			//Conecta con el servidor POP
			almacenamientoMails.connect();
			
			//Creo una carpeta y guardo todo el inbox
			Folder carpetaMails = almacenamientoMails.getFolder("INBOX");
			
			//Seteo los parametros de la carpeta
			carpetaMails.open(Folder.READ_ONLY);
			
			//Guardo todos los mails del Inbox
			this.guardarMails(carpetaMails);
						
		    // Cierro la carpeta
		    carpetaMails.close(false);
		    
		    //Cierro el almacenamiento de mails
		    almacenamientoMails.close();
		}
		catch (NoSuchProviderException nspe) 
		{
			nspe.printStackTrace();
			throw new RecepcionFallidaException("No existe el proveedor");   
	    } 
		catch (FolderNotFoundException fnfe) 
		{
	         fnfe.printStackTrace();
	         throw new RecepcionFallidaException("No se encontro la carpeta");  
	    } 
		catch(AuthenticationFailedException afe)
		{
			  afe.printStackTrace();
			  throw new LoginException("Error en el logueo."); 
		}
		catch(NullPointerException npe)
		{
			npe.printStackTrace();
			throw new RecepcionFallidaException("No hay mails para traer.");
		}
		catch (MailException me) 
		{
	    	  throw(me);
		}
		catch (DAOException daoe) 
		{
	    	  throw(daoe);
		}
		catch (Exception e) 
		{
	    	  System.out.println("Error al recibir mails "+e);
	    	  throw new RecepcionFallidaException("Error no esperado al recibir un correo"); 
	    }
	}
	
	//Guarda los correos recibidos en una lista
	private void guardarMails(Folder carpetaMails) throws MailException, DAOException 
	{
		try 
		{
			this.listaInboxRecibidos = new ArrayList<Message>(Arrays.asList(carpetaMails.getMessages()));
		} 
		catch (MessagingException e) 
		{
			e.printStackTrace();
			throw new MailException("Hubo un problema al obtener todos los mensajes del mail.", e);	
		}
		
		this.listaMails = new ArrayList<Mail>(); // ESTE TIENE QUE VOLAR; DESPUES VA CUANDO LEE DE LA BASE DE DATOS
		
		for (Message mailDescargado : this.listaInboxRecibidos) 
		{
			try
			{			
				String origen = ((InternetAddress)mailDescargado.getFrom()[0]).getAddress();
				String destinatario = ((InternetAddress)mailDescargado.getAllRecipients()[0]).getAddress();
				String asunto = mailDescargado.getSubject();
				String mensaje = this.obtenerTextoDelMensaje(mailDescargado);
		        
		        Mail nuevoMail = new Mail(origen,destinatario,asunto,mensaje);
		        
		        if(!origen.equals(this.usuario.getNombreUsuario()))
		        	Mail.guardarMailRecibido(nuevoMail);
		        else
		        	Mail.guardarMailEnviado(nuevoMail);
		           
			}
			catch(IOException | MessagingException  e)
			{
				e.printStackTrace();
				throw new MailException("Hubo un problema al obtener el cuerpo del mail", e);
			}
			catch(DAOException daoe)
			{
				daoe.printStackTrace();
				throw(daoe);
			}
		}
		
		System.out.println("Cantidad de mensajes guardados: "+this.listaInboxRecibidos.size());
		this.leerMailsBaseDeDatos();
	}

	//Muestra por consola los mails del INBOX
	private void mostrarListadoRecibidoPorConsola() throws MessagingException, IOException
	{
		for (Message unMail : this.listaInboxRecibidos) 
		{
		   System.out.println("\n\n---------------------------------");
	       System.out.println("De: " + unMail.getFrom()[0]);
	       System.out.println("Asunto: " + unMail.getSubject());

	       //System.out.println("Mensaje: " + unMail.getContent().toString());
	       if (unMail.getContent() instanceof String)  
	       {  
	           String body = (String)unMail.getContent(); 
	           System.out.println("--------------------");
	           System.out.println("STRING");
	           System.out.println("--------------------");
	           System.out.println(body);
	       }  
	       else if (unMail.getContent() instanceof Multipart)  
	       {  
	           Multipart mp = (Multipart)unMail.getContent();
	           System.out.println("--------------------");
	           System.out.println("MULTIPART");
	           System.out.println("--------------------");
	           System.out.println(this.obtenerTextoDelMensaje(unMail));
	       } 
		}
	}
	
	/*METODOS PARA OBTENER EL MENSAJE DEL MAIL*/
	private String obtenerTextoDelMensaje(Message mensaje) throws MessagingException, IOException {
	    String result = "";
	    if (mensaje.isMimeType("text/plain")) {
	        result = mensaje.getContent().toString();
	    } else if (mensaje.isMimeType("multipart/*")) {
	        MimeMultipart mimeMultipart = (MimeMultipart) mensaje.getContent();
	        result = this.getTextoDeMultipart(mimeMultipart);
	    }
	    return result;
	}

	/*METODOS PARA OBTENER EL MENSAJE DEL MAIL CUADNO ES MULTIPART*/
	private String getTextoDeMultipart(MimeMultipart mimeMultipart)  throws MessagingException, IOException{
	    String result = "";
	    int count = mimeMultipart.getCount();
	    for (int i = 0; i < count; i++) {
	        BodyPart bodyPart = mimeMultipart.getBodyPart(i);
	        if (bodyPart.isMimeType("text/plain")) {
	            result = result + "\n" + bodyPart.getContent();
	            break; // without break same text appears twice in my tests
	        } else if (bodyPart.isMimeType("text/html")) {
	            String html = (String) bodyPart.getContent();
	            result = result + "\n" + Jsoup.parse(html).text();
	        } else if (bodyPart.getContent() instanceof MimeMultipart){
	            result = result + this.getTextoDeMultipart((MimeMultipart)bodyPart.getContent());
	        }
	    }
	    return result;
	}
	
	
	
	private void mostrarListadoYaProcesadosPorConsola()
	{
		for (Mail unMail : this.listaMails) 
		{
			System.out.println("\n\n--------------------------");
			System.out.println("De: "+unMail.getOrigen());
			System.out.println("Para: "+unMail.getDestinatario());
			System.out.println("Asunto: "+unMail.getAsunto());
			System.out.println("Mensaje: \n"+unMail.getMensaje());
		}
	}
	
	//Marco el correo como borrado
	public void borrarCorreo(Mail unMail) throws DAOException
	{
		this.marcarCorreoComo(this.tagBorrado, unMail);
	}

	//Marco el correo segun el tag que reciba
	public void marcarCorreoComo(String tag, Mail unMail) throws DAOException
	{
		unMail.setTag(tag);

		this.dao.modificarMail(unMail);
	}
	
	//Lee los mails de la base de datos y los pone en sus respectivas carpetas
	private void leerMailsBaseDeDatos() throws DAOException
	{
		this.dao = new MailDaoH2Impl();
		this.listaMails = this.dao.listarMails();
		
		//this.mostrarListadoYaProcesadosPorConsola();
	}

	public ArrayList<Contacto> traerContactos() throws DAOException 
	{
		try 
		{
			ContactoDAO dao = new ContactoDaoH2Impl();	
			this.listaContactos = dao.listarContactos();
		} 
		catch (DAOException daoe) 
		{
			
			throw(daoe);
		}
		
		return this.listaContactos;
	}
	
	
	public ArrayList<Mail> dameMailsConTag(String tag)
	{
		ArrayList<Mail> coincidencias = new ArrayList<Mail>();
		
		for (Mail unMail : this.listaMails) 
		{
			if(unMail.getTag().equals(tag))
				coincidencias.add(unMail);				
		}
		
		return coincidencias;
	}
	
}
