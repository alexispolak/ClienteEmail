package modelo;

import javax.swing.JOptionPane;

import dao.ContactoDAO;
import dao.impl.ContactoDaoH2Impl;
import exceptions.DAOException;

public class Contacto extends Persona
{

	//ATRIBUTOS
	private String direccion;
	private int id;

	//CONSTRUCTORES
	public Contacto()
	{
		super();
		System.out.println("");
	}
	
	public Contacto(String nombre, String apellido)
	{
		super(nombre, apellido);
	}
	
	public Contacto(String nombre, String apellido, String direccion)
	{
		super(nombre, apellido);
		this.direccion = direccion;
	}
	
	public Contacto(int id, String nombre, String apellido, String direccion)
	{
		super(nombre, apellido);
		this.direccion = direccion;
		this.id = id;
	}
	
	
	//GETTERS
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	//METODOS	
	public String toString()
	{
		return this.nombre+" "+this.apellido+" "+this.direccion;
	}

	public static void agregarContacto() throws DAOException 
	{
		try 
		{
			Contacto nuevoContacto = new Contacto("","","");
            ContactoDAO dao = new ContactoDaoH2Impl();
            dao.crearContacto(nuevoContacto);
        } 
		catch (DAOException daoe) 
		{
            throw(daoe);
        }
		
	}

	public static void borrar(Contacto contacto) throws DAOException 
	{
		try 
		{
			ContactoDAO dao = new ContactoDaoH2Impl();
            dao.borrarContacto(contacto);
        } 
		catch (DAOException daoe) 
		{
            throw(daoe);
        }
	}
	
	public static void modificarContacto(Contacto contacto) throws DAOException 
	{
		try
		{
			ContactoDAO dao = new ContactoDaoH2Impl();
            dao.modificarContacto(contacto);
		}
		catch(DAOException daoe)
		{
			throw(daoe);
		}
	}
}
