package modelo;

import javax.swing.JOptionPane;

import dao.ConfiguracionPOP3DAO;
import dao.ConfiguracionSMTPDAO;
import dao.impl.ConfiguracionPop3DaoImpl;
import dao.impl.ConfiguracionSmtpDaoImpl;
import exceptions.DAOException;

public class ConfiguracionSMTP extends Configuracion
{
	//ATRIBUTOS
	private String autorizacion;
	private ConfiguracionSMTPDAO dao;
	
	//CONSTRUCTORES
	public ConfiguracionSMTP() throws DAOException
	{
		this.leerConfiguracion();
	}
	
	public ConfiguracionSMTP(String host, String puerto, String starttls, String autorizacion)
	{
		super(host, puerto, starttls);
		this.autorizacion = autorizacion;
	}
	
	//GETTERS
	public String getAutorizacion() {
		return autorizacion;
	}
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}
	
	//METODOS
	public String toString()
	{
		return "Host: "+this.puerto+" Port: "+this.puerto+" starttls.enable: "+this.starttls+ " Auth: "+this.autorizacion;
	}

	//Lee la configuracion de la base de datos
	public void leerConfiguracion() throws DAOException 
	{
		try
		{
			this.dao = new ConfiguracionSmtpDaoImpl();
			ConfiguracionSMTP confTemporal = this.dao.consultarConfiguracionSmtp();
			this.autorizacion = confTemporal.autorizacion;
			this.host = confTemporal.host;
			this.puerto = confTemporal.puerto;
			this.starttls = confTemporal.starttls;
		}
		catch(DAOException daoe)
		{
			this.crearNuevaConfiguracion(new ConfiguracionSMTP("smtp.gmail.com", "25", "true", "true")); //CREO CON PARAMETROS POR DEFECTO 
		}
	}
	
	//Solo debe usarse si no se encuentra una en la base de datos
	public void crearNuevaConfiguracion(ConfiguracionSMTP unConfiguracionSMTP) throws DAOException
	{
        ConfiguracionSMTPDAO dao = new ConfiguracionSmtpDaoImpl();
        dao.crearConfiguracionSmtp(unConfiguracionSMTP);
	}
	
	public static void actualizarConfiguracionSMTP(String host, String puerto, String starttls, String autorizacion) throws DAOException
	{
		ConfiguracionSMTP modificado = new ConfiguracionSMTP(host,puerto,starttls,autorizacion);
		modificado.setHost(modificado.getHost());
		modificado.setPuerto(modificado.getPuerto());
		modificado.setStarttls(modificado.getStarttls());
		modificado.setAutorizacion(modificado.getAutorizacion());
		
		try 
		{
			//PERSISTENCIA DE CONFIGURACION SMTP
			ConfiguracionSMTPDAO dao = new ConfiguracionSmtpDaoImpl();
            dao.modificarConfiguracionSmtp(modificado);
        } 
		catch (DAOException daoe) 
		{
            throw(daoe);
        }
	}
}
