package modelo;

import javax.swing.JOptionPane;

import dao.ConfiguracionPOP3DAO;
import dao.ConfiguracionSMTPDAO;
import dao.impl.ConfiguracionPop3DaoImpl;
import dao.impl.ConfiguracionSmtpDaoImpl;
import exceptions.DAOException;

public class ConfiguracionPOP3 extends Configuracion
{
	//ATRIBUTOS
	private ConfiguracionPOP3DAO dao;
	
	//CONSTRUCTORES
	public ConfiguracionPOP3()  throws DAOException 
	{
		super();
		this.leerConfiguracion();
	}
	
	public ConfiguracionPOP3(String host, String puerto, String starttls)
	{
		super(host, puerto, starttls);
	}
	
	//GETTERS
	
	//METODOS
	public String toString()
	{
		return "Host: "+this.puerto+" Port: "+this.puerto+" starttls.enable: "+this.starttls;
	}
	

	//Lee la configuracion de la base de datos
	public void leerConfiguracion() throws DAOException 
	{
		try
		{
			this.dao = new ConfiguracionPop3DaoImpl();
			ConfiguracionPOP3 confTemporal = this.dao.consultarConfiguracionPop3();
			this.host = confTemporal.host;
			this.puerto = confTemporal.puerto;
			this.starttls = confTemporal.starttls;
		}
		catch(DAOException daoe)
		{
			this.crearNuevaConfiguracion( new ConfiguracionPOP3("pop.gmail.com", "995", "true")); //CREO CON PARAMETROS POR DEFECTO
		}
	}

	private void crearNuevaConfiguracion(ConfiguracionPOP3 configuracionPOP3) throws DAOException 
	{
        ConfiguracionPOP3DAO dao = new ConfiguracionPop3DaoImpl();
        dao.crearConfiguracionPop3(configuracionPOP3);	
	}
	
	public static void actualizarConfiguracionPOP3(String host, String puerto, String starttls) throws DAOException
	{
		ConfiguracionPOP3 modificado = new ConfiguracionPOP3(host, puerto, starttls);
		modificado.setHost(modificado.getHost());
		modificado.setPuerto(modificado.getPuerto());
		modificado.setStarttls(modificado.getStarttls());
		
		try 
		{
			//PERSISTENCIA DE CONFIGURACION POP3
            ConfiguracionPOP3DAO dao = new ConfiguracionPop3DaoImpl();
            dao.modificarConfiguracionPop3(modificado);
        } 
		catch (DAOException daoe) 
		{
            throw(daoe);
        }
	}
	
	
}
