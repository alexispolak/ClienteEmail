package modelo;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import dao.ContactoDAO;
import dao.MailDAO;
import dao.impl.ContactoDaoH2Impl;
import dao.impl.MailDaoH2Impl;
import exceptions.DAOException;

import javax.mail.Authenticator;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;

public class Mail 
{
	//ATRIBUTOS
	private String origen;
	private String destinatario;
	private String asunto;
	private String mensaje;
	private String tag;
	private int id;
	
	//CONSTRUCTORES
	public Mail()
	{
		System.out.println("");
	}
	
	public Mail(String origen, String destinatario, String asunto, String mensaje)
	{
		this.origen = origen;
		this.destinatario = destinatario;
		this.asunto = asunto;
		this.mensaje = mensaje;
		this.tag = "Nuevo";
	}
	
	public Mail(int id, String origen, String destinatario, String asunto, String mensaje)
	{
		this.origen = origen;
		this.destinatario = destinatario;
		this.asunto = asunto;
		this.mensaje = mensaje;
		this.tag = "Nuevo";
		this.id = id;
	}
	
	//GETTERS
	public String getOrigen() {
		return origen;
	}
	public void setOrigen(String origen) {
		this.origen = origen;
	}
	public String getDestinatario() {
		return destinatario;
	}
	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getAsunto() {
		return asunto;
	}
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag){
		this.tag = tag;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	//METODOS
	public String toString()
	{
		return "Mail de:"+this.origen+" para:"+this.destinatario+"\n"+this.mensaje;
	}

	public static void guardarMailRecibido(Mail unMail) throws DAOException
	{
		try 
		{
			MailDAO dao = new MailDaoH2Impl();
			unMail.setTag("Recibido");
			dao.crearMail(unMail);
		} 
		catch (DAOException daoe)
		{
			daoe.printStackTrace();
			throw(daoe);
		}
	}
	
	public static void guardarMailEnviado(Mail unMail) throws DAOException
	{
		try 
		{
			MailDAO dao = new MailDaoH2Impl();
			unMail.setTag("Enviado");
			dao.crearMail(unMail);
		} 
		catch (DAOException daoe)
		{
			daoe.printStackTrace();
			throw(daoe);
		}

	}
	
	public static void eliminar(Mail unMail) throws DAOException
	{
		try 
		{
			MailDAO dao = new MailDaoH2Impl();
			unMail.setTag("Eliminado");
			dao.modificarMail(unMail);
        } 
		catch (DAOException daoe) 
		{
            throw(daoe);
        }
	}
}
