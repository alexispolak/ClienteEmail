package modelo;

public class Persona 
{
	//ATRIBUTOS
	protected String nombre;
	protected String apellido;
	
	//CONSTRUCTORES
	public Persona()
	{
		System.out.println("");
	}
	
	public Persona(String nombre, String apellido)
	{
		this.setNombre(nombre);
		this.setApellido(apellido);
	}

	//GETTERS Y SETTERS
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	//METODOS
	public String toString()
	{
		return this.nombre+" "+this.apellido;
	}
	
}
