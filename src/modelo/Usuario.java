package modelo;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import dao.ConfiguracionSMTPDAO;
import dao.UsuarioDAO;
import dao.impl.ConfiguracionSmtpDaoImpl;
import dao.impl.UsuarioDaoH2Impl;
import exceptions.DAOException;

public class Usuario extends Persona
{

	//ATRIBUTOS
	private String nombreUsuario;
	private String contrasenia;
	
	private UsuarioDAO dao;
	
	//CONSTRUCTORES
	public Usuario() throws DAOException
	{
		super();
		this.leerUsuario();
	}
	
	public Usuario(String nombre, String apellido, String nombreUsuario, String contrasenia)
	{
		super(nombre, apellido);
		this.nombreUsuario = nombreUsuario;
		this.contrasenia = contrasenia;
		
	}
	
	//GETTERS
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getContrasenia() {
		return contrasenia;
	}
	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}	
	
	//Lee al usuario de la base de datos
	public void leerUsuario() throws DAOException 
	{
		try 
		{
            UsuarioDAO dao = new UsuarioDaoH2Impl();
            ArrayList<Usuario> listaUsuario = dao.listarUsuarios();
                       
            this.nombreUsuario = listaUsuario.get(0).nombreUsuario;
            this.contrasenia = listaUsuario.get(0).contrasenia;
            this.nombre = listaUsuario.get(0).nombre;
            this.apellido = listaUsuario.get(0).apellido;
   
        } 
		catch (DAOException daoe) 
		{
			this.crearNuevoUsuario();
        }
		catch (IndexOutOfBoundsException oobe)
		{
			this.crearNuevoUsuario();
		}
 	}
	
	private void crearNuevoUsuario() throws DAOException 
	{
		UsuarioDAO dao = new UsuarioDaoH2Impl();
        dao.crearUsuario(new Usuario("Harambe", "McHarambe Jr", "pruebamaillabouno@gmail.com", "laboratorioUno"));	
	}
	
	public static void actualizarUsuario(String nombreUsuario, String pass) throws DAOException
	{
		Usuario modificado = new Usuario("","",nombreUsuario,pass);
		modificado.setApellido(modificado.getApellido());
		modificado.setNombre(modificado.getNombre());
		modificado.setContrasenia(modificado.getContrasenia());
		
		try 
		{
            UsuarioDAO dao = new UsuarioDaoH2Impl();
            dao.modificarUsuario(modificado);
        } 
		catch (DAOException daoe) 
		{
            throw(daoe);
        }
	}

	//METODOS
	public String toString()
	{
		return this.nombre+" "+this.apellido+" - usuario: "+this.nombreUsuario+" contrasenia: "+this.contrasenia;
	}
}
