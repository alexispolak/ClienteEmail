package modelo;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public class MailTableModel  extends AbstractTableModel 
{
	//ATRIBUTOS
	//Indices de las columnas
	private static final int COLUMNA_ORIGEN = 0;
	private static final int COLUMNA_ASUNTO = 1;
	private static final int COLUMNA_MENSAJE = 2;
	
	//Nombres de los encabezados
	private String[] nombresColumnas = {"Origen", "Asunto", "Mensaje"};
	
	//Tipos de cada columna
	private Class[] tiposColumnas = {String.class, String.class, String.class};

	//Filas
	private List<Mail> filas;
	
	
	//CONSTRUCTORES
	public MailTableModel() 
	{
		this.filas = new ArrayList<Mail>();
	}
	
	public MailTableModel(List<Mail> listaMailsInicial) {
		this.filas = listaMailsInicial;
	}

	/**
	 * METODO QUE HAY QUE PISAR
	 */
	public int getColumnCount() {
		return nombresColumnas.length;
	}

	/**
	 * OTRO METODO QUE HAY QUE PISAR
	 */
	public int getRowCount() {
		return filas.size();
	}

	/**
	 * METODO QUE HAY QUE PISAR ---------------- SI QUIERO EDITAR POR CELDAS!
	 */
	/*public void setValueAt(Object value, int row, int col) {
		
		    Mail unMail = this.filas.get(row);
			
			Object result = null;
			switch(col) {
			case COLUMNA_ORIGEN:
				unMail.setNombre((String)value);
				break;
			case COLUMNA_ASUNTO:
				unMail.setApellido((String)value);
				break;
			case COLUMNA_MENSAJE:
				unMail.setDireccion((String)value);
				break;
			}
			
			fireTableCellUpdated(row,col);
	}*/
	
	//Metodo que devuelve los valores obtenidos
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		Mail unMail = this.filas.get(rowIndex);
		
		Object result = null;
		switch(columnIndex) {
		case COLUMNA_ORIGEN:
			result = unMail.getOrigen();
			break;
		case COLUMNA_ASUNTO:
			result = unMail.getAsunto();
			break;
		case COLUMNA_MENSAJE:
			result = unMail.getMensaje();
			break;
		default:
			result = new String("");
		}
		
		return result;
	}

	/**
	 * METODO QUE HAY QUE PISAR
	 */
	public String getColumnName(int col) {
        return nombresColumnas[col];
    }

	/**
	 * METODO QUE HAY QUE PISAR
	 */
    public Class getColumnClass(int col) {
        return tiposColumnas[col];
    }
    
    
    /**
     * GETTER DE MIS FILAS
     * @return
     */
    public List<Mail> getFilas() {
    	return this.filas;
    }

    /**
     * SETTER DE MIS FILAS 
     * 
     * @param filas
     */
    public void setFilas(List<Mail> filas) {
    	this.filas = filas;
    }
    
    /**
     * AGREGO PARA PODEER EDITAR CELDAS!!
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex){
        return true;
    }
}