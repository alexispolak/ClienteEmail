package modelo;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public class ContactosTableModel  extends AbstractTableModel 
{
	//ATRIBUTOS
	//Indices de las columnas
	private static final int COLUMNA_NOMBRE = 0;
	private static final int COLUMNA_APELLIDO = 1;
	private static final int COLUMNA_DIRECCION = 2;
	
	//Nombres de los encabezados
	private String[] nombresColumnas = {"Nombre", "Apellido", "Direccion"};
	
	//Tipos de cada columna
	private Class[] tiposColumnas = {String.class, String.class, String.class};

	//Filas
	private List<Contacto> filas;
	
	
	//CONSTRUCTORES
	public ContactosTableModel() 
	{
		this.filas = new ArrayList<Contacto>();
	}
	
	public ContactosTableModel(List<Contacto> listaContactosInicial) {
		this.filas = listaContactosInicial;
	}

	/**
	 * METODO QUE HAY QUE PISAR
	 */
	public int getColumnCount() {
		return nombresColumnas.length;
	}

	/**
	 * OTRO METODO QUE HAY QUE PISAR
	 */
	public int getRowCount() {
		return filas.size();
	}

	/**
	 * METODO QUE HAY QUE PISAR ---------------- SI QUIERO EDITAR POR CELDAS!
	 */
	public void setValueAt(Object value, int row, int col) {
		
		    Contacto unContacto = this.filas.get(row);
			
			Object result = null;
			switch(col) {
			case COLUMNA_NOMBRE:
				unContacto.setNombre((String)value);
				break;
			case COLUMNA_APELLIDO:
				unContacto.setApellido((String)value);
				break;
			case COLUMNA_DIRECCION:
				unContacto.setDireccion((String)value);
				break;
			}
			
			fireTableCellUpdated(row,col);
	}
	
	//Metodo que devuelve los valores obtenidos
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		Contacto unContacto = this.filas.get(rowIndex);
		
		Object result = null;
		switch(columnIndex) {
		case COLUMNA_NOMBRE:
			result = unContacto.getNombre();
			break;
		case COLUMNA_APELLIDO:
			result = unContacto.getApellido();
			break;
		case COLUMNA_DIRECCION:
			result = unContacto.getDireccion();
			break;
		default:
			result = new String("");
		}
		
		return result;
	}

	/**
	 * METODO QUE HAY QUE PISAR
	 */
	public String getColumnName(int col) {
        return nombresColumnas[col];
    }

	/**
	 * METODO QUE HAY QUE PISAR
	 */
    public Class getColumnClass(int col) {
        return tiposColumnas[col];
    }
    
    
    /**
     * GETTER DE MIS FILAS
     * @return
     */
    public List<Contacto> getFilas() {
    	return this.filas;
    }

    /**
     * SETTER DE MIS FILAS 
     * 
     * @param filas
     */
    public void setFilas(List<Contacto> filas) {
    	this.filas = filas;
    }
    
    /**
     * AGREGO PARA PODEER EDITAR CELDAS!!
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex){
        return true;
    }
}