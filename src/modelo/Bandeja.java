package modelo;

import java.util.ArrayList;

public class Bandeja 
{
	//TODO esta clase no va ma me parece
	//ATRIBUTOS
	private String nombre;
	private ArrayList<Mail> listaMails;
	
	//CONSTRUCTORES
	public Bandeja()
	{
		System.out.println("Esta bandeja no tiene nombre");
		this.setearCampos(null, new ArrayList<Mail>());
	}
	
	public Bandeja(String nombre)
	{
		this.setearCampos(nombre, new ArrayList<Mail>());
	}
	
	public Bandeja(String nombre, ArrayList<Mail> listaMails)
	{
		this.setearCampos(nombre, listaMails);
	}
	
	//Setea los atributos de la clase
	private void setearCampos(String nombre, ArrayList<Mail> listaMails)
	{
		this.nombre = nombre;
		this.listaMails = listaMails;
	}
	
	//GETTERS
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public ArrayList<Mail> getListaMails() {
		return listaMails;
	}
	public void setListaMails(ArrayList<Mail> listaMails) {
		this.listaMails = listaMails;
	}
	
	//METODOS
	
	public String toString()
	{
		String nombre = this.nombre+" tiene "+this.listaMails.size()+" correos\n";
		String lista = "";
		for (Mail unMail : this.listaMails) {
			lista += unMail.toString()+"\n";
		}
		String fin = "---------Fin lista de Mails---------";
		return nombre+lista+fin;
	}

	
}
