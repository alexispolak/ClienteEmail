package dao.impl;

import dao.UsuarioDAO;
import dataManager.DBManager;
import dataManager.DBManagerUsuario;
import exceptions.DBException;
import exceptions.DAOException;
import modelo.Usuario;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UsuarioDaoH2Impl implements UsuarioDAO 
{
	private final String tipo = "Usuario";
	private DBManager dbManager = new DBManagerUsuario();
	
    @Override
    public void crearUsuario(Usuario unUsuario) throws DAOException 
    {	
        String nombre = unUsuario.getNombre();
        String apellido = unUsuario.getApellido();
        String usuario = unUsuario.getNombreUsuario();
        String pass = unUsuario.getContrasenia();

        String sql = "INSERT INTO usuarios (nombre, apellido, usuario, pass) VALUES ('" + nombre + "', '" + apellido + "', '" + usuario + "', '" + pass + "')";
        
        try
        {
        	this.dbManager.ejecutarConsultaAbmSqlH2(sql, "Insertar", this.tipo);
        }
        catch(DAOException daoe)
        {
        	throw new DAOException(daoe.getMessage(),daoe);
        }
    }

    @Override
    public void borrarUsuario(Usuario unUsuario) throws DAOException 
    {
        String usuario = unUsuario.getNombreUsuario();
        
        String sql = "DELETE FROM usuarios WHERE usuario = '" + usuario + "'";
        
        try
        {
        	this.dbManager.ejecutarConsultaAbmSqlH2(sql, "Borrar", this.tipo);
        }
        catch(DAOException daoe)
        {
        	throw new DAOException(daoe.getMessage(),daoe);
        }
    }
    

    @Override
    public void modificarUsuario(Usuario unUsuario) throws DAOException 
    {
    	String nombre = unUsuario.getNombre();
        String apellido = unUsuario.getApellido();
        String usuario = unUsuario.getNombreUsuario();
        String pass = unUsuario.getContrasenia();

        String sql = "UPDATE usuarios set nombre = '" + nombre + "', apellido = '" + apellido +"', pass = '" + pass + "' WHERE usuario = '" + usuario + "'";
        
        try
        {
        	this.dbManager.ejecutarConsultaAbmSqlH2(sql, "Modificar", this.tipo);
        }
        catch(DAOException daoe)
        {
        	throw new DAOException(daoe.getMessage(),daoe);
        }
    }

    @Override
    public ArrayList<Usuario> listarUsuarios() throws DAOException 
    {	
        ArrayList<Usuario> listaUsuarios = new ArrayList<Usuario>();
        
        String sql = "SELECT * FROM usuarios";
        
        try
        {
        	listaUsuarios = (ArrayList<Usuario>) this.dbManager.ejecutarConsultaListadoSqlH2(sql, "Listar Todos", this.tipo);
        	
        	/*for (Usuario usuario : listaUsuarios) {
				System.out.println(usuario.toString());
			}*/
        }
        catch(DAOException daoe)
        {
        	throw new DAOException(daoe.getMessage(),daoe);
        }
        
        return listaUsuarios;
    }

    @Override
    public Usuario consultarUsuario(String nombreUsuario) throws DAOException 
    {
        Usuario usuario = new Usuario();
        String sql = "SELECT * FROM usuarios WHERE usuario = '" + nombreUsuario + "'";
        
        try
        {
        	usuario = ((ArrayList<Usuario>) this.dbManager.ejecutarConsultaListadoSqlH2(sql, "Listar Uno", this.tipo)).get(0);

        }
        catch(DAOException daoe)
        {
        	throw new DAOException(daoe.getMessage(),daoe);
        }        
       
        return usuario;
    }
}
