package dao.impl;

import java.util.ArrayList;

import dao.MailDAO;
import dataManager.DBManager;
import dataManager.DBManagerContacto;
import dataManager.DBManagerMail;
import exceptions.DAOException;
import modelo.Contacto;
import modelo.Mail;

public class MailDaoH2Impl implements MailDAO
{
	private final String tipo = "Mail";
	private DBManager dbManager = new DBManagerMail();
	
	@Override
	public void crearMail(Mail mail) throws DAOException 
	{
		String origen = mail.getOrigen();
        String destinatario = mail.getDestinatario();
        String asunto = mail.getAsunto();
        String mensaje = mail.getMensaje();
        String tag = mail.getTag();

        String sql = "INSERT INTO mails (origen, destinatario, asunto, mensaje, tag) VALUES ('" + origen + "', '" + destinatario + "', '" + asunto+ "', '" + mensaje+ "', '" + tag + "')";
        
        try
        {
        	this.dbManager.ejecutarConsultaAbmSqlH2(sql, "Insertar", this.tipo);
        }
        catch(DAOException daoe)
        {
        	throw new DAOException(daoe.getMessage(),daoe);
        }
	}

	@Override
	//El mail solo puede modificar su tag. Si se "BORRA" se le agrega el tag: Eliminado.
	public void modificarMail(Mail mail) throws DAOException 
	{
        String tag = mail.getTag();
        int id = mail.getId();
        
        String sql = "UPDATE mails set tag = '" + tag + "' WHERE id = '" + id + "'";
        
        try
        {
        	this.dbManager.ejecutarConsultaAbmSqlH2(sql, "Modificar", this.tipo);
        }
        catch(DAOException daoe)
        {
        	throw new DAOException(daoe.getMessage(),daoe);
        }
	}

	@Override
	public ArrayList<Mail> listarMails() throws DAOException 
	{
		ArrayList<Mail> listaMails = new ArrayList<Mail>();
        
        String sql = "SELECT * FROM mails";
        
        try
        {
        	listaMails = (ArrayList<Mail>) this.dbManager.ejecutarConsultaListadoSqlH2(sql, "Listar Todos", this.tipo);
        }
        catch(DAOException daoe)
        {
        	throw new DAOException(daoe.getMessage(),daoe);
        }
        
        
        return listaMails;
	}

}
