package dao.impl;

import java.util.ArrayList;

import dao.ConfiguracionSMTPDAO;
import dataManager.DBManager;
import dataManager.DBManagerConfiguracionSMTP;
import exceptions.DAOException;
import modelo.ConfiguracionPOP3;
import modelo.ConfiguracionSMTP;

public class ConfiguracionSmtpDaoImpl implements ConfiguracionSMTPDAO
{
	private String tipo = "ConfiguracionSMTP";
	private DBManager dbManager = new DBManagerConfiguracionSMTP();
	
	@Override
	public void crearConfiguracionSmtp(ConfiguracionSMTP configuracion) throws DAOException 
	{
		String host = configuracion.getHost();
		String puerto = configuracion.getPuerto();
		String starttls = configuracion.getStarttls();
		String autorizacion = configuracion.getAutorizacion();

        String sql = "INSERT INTO configuracionSmtp (host, puerto, starttls, autorizacion) VALUES ('" + host + "', '" + puerto + "', '" + starttls+ "', '" + autorizacion  + "')";
        
        try
        {
        	this.dbManager.ejecutarConsultaAbmSqlH2(sql, "Insertar", this.tipo);
        }
        catch(DAOException daoe)
        {
        	throw new DAOException(daoe.getMessage(),daoe);
        }
		
	}

	@Override
	public void modificarConfiguracionSmtp(ConfiguracionSMTP configuracion) throws DAOException 
	{
		String host = configuracion.getHost();
		String puerto = configuracion.getPuerto();
		String starttls = configuracion.getStarttls();
		String autorizacion = configuracion.getAutorizacion();

        String sql = "UPDATE configuracionSmtp set host = '" + host + "', puerto = '" + puerto +"', starttls = '" + starttls+ "', autorizacion = '" + autorizacion + "'";
        
        try
        {
        	this.dbManager.ejecutarConsultaAbmSqlH2(sql, "Modificar", this.tipo);
        }
        catch(DAOException daoe)
        {
        	throw new DAOException(daoe.getMessage(),daoe);
        }	
	}

	@Override
	public ConfiguracionSMTP consultarConfiguracionSmtp() throws DAOException 
	{
        String sql = "SELECT * FROM configuracionSmtp";
        
        try
        {
        	return ((ArrayList<ConfiguracionSMTP>) this.dbManager.ejecutarConsultaListadoSqlH2(sql, "Listar Uno", this.tipo)).get(0);
        }
        catch(DAOException daoe)
        {
        	throw new DAOException(daoe.getMessage()+"\nNo se puedo cargar la configuracion SMTP",daoe);
        }
        catch(IndexOutOfBoundsException ioobe)
        {
        	throw new DAOException("\nNo hay almacenada una configuracion SMTP.\nIngrese a la configuracion para agregarla.",ioobe);
        }
	}

}
