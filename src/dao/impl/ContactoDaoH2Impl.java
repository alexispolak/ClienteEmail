package dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import dao.ContactoDAO;
import dataManager.DBManager;
import dataManager.DBManagerContacto;
import exceptions.DAOException;
import modelo.Contacto;

public class ContactoDaoH2Impl implements ContactoDAO
{
	private final String tipo = "Contacto";
	private DBManager dbManager = new DBManagerContacto();
	
    @Override
    public void crearContacto(Contacto unContacto) throws DAOException 
    {	
        String nombre = unContacto.getNombre();
        String apellido = unContacto.getApellido();
        String direccion = unContacto.getDireccion();

        String sql = "INSERT INTO contactos (nombre, apellido, direccion) VALUES ('" + nombre + "', '" + apellido + "', '" + direccion  + "')";
        
        try
        {
        	this.dbManager.ejecutarConsultaAbmSqlH2(sql, "Insertar", this.tipo);
        }
        catch(DAOException daoe)
        {
        	throw new DAOException(daoe.getMessage(),daoe);
        }
    }

    @Override
    public void borrarContacto(Contacto unContacto) throws DAOException 
    {
        int id = unContacto.getId();
        
        String sql = "DELETE FROM contactos WHERE id = '" + id + "'";
        
        try
        {
        	this.dbManager.ejecutarConsultaAbmSqlH2(sql, "Borrar", this.tipo);
        }
        catch(DAOException daoe)
        {
        	throw new DAOException(daoe.getMessage(),daoe);
        }
    }
    
    @Override
    public void modificarContacto(Contacto unContacto) throws DAOException 
    {
    	String nombre = unContacto.getNombre();
        String apellido = unContacto.getApellido();
        String direccion = unContacto.getDireccion();
        int id = unContacto.getId();

        String sql = "UPDATE contactos set nombre = '" + nombre + "', apellido = '" + apellido +"', direccion = '" + direccion + "' WHERE id = '" + id + "'";
        
        try
        {
        	this.dbManager.ejecutarConsultaAbmSqlH2(sql, "Modificar", this.tipo);
        }
        catch(DAOException daoe)
        {
        	throw new DAOException(daoe.getMessage(),daoe);
        }
    }

    @Override
    public ArrayList<Contacto> listarContactos() throws DAOException 
    {	
        ArrayList<Contacto> listaContactos = new ArrayList<Contacto>();
        
        String sql = "SELECT * FROM contactos";
        
        try
        {
        	listaContactos = (ArrayList<Contacto>) this.dbManager.ejecutarConsultaListadoSqlH2(sql, "Listar Todos", this.tipo);
        	
        	/*for (Contacto contacto : listaContactos) {
				System.out.println(contacto.toString());
			}*/
        }
        catch(DAOException daoe)
        {
        	throw new DAOException(daoe.getMessage(),daoe);
        }
        
        return listaContactos;
    }

    @Override
    public Contacto consultarContacto(String direccionContacto) throws DAOException 
    {
        Contacto contacto = new Contacto();
        String sql = "SELECT * FROM contactos WHERE contacto = '" + direccionContacto + "'";
        
        try
        {
        	contacto = ((ArrayList<Contacto>) this.dbManager.ejecutarConsultaListadoSqlH2(sql, "Listar Uno", this.tipo)).get(0);
        	
        	//System.out.println(contacto.toString());
        }
        catch(DAOException daoe)
        {
        	throw new DAOException(daoe.getMessage(),daoe);
        }        
       
        return contacto;
    }
}
