package dao.impl;

import java.util.ArrayList;

import dao.ConfiguracionPOP3DAO;
import dataManager.DBManager;
import dataManager.DBManagerConfiguracionPop3;
import exceptions.DAOException;
import modelo.ConfiguracionPOP3;
import modelo.Contacto;

public class ConfiguracionPop3DaoImpl implements ConfiguracionPOP3DAO
{
	private String tipo = "ConfiguracionPOP3";
	private DBManager dbManager = new DBManagerConfiguracionPop3();
	
	@Override
	public void crearConfiguracionPop3(ConfiguracionPOP3 configuracion) throws DAOException 
	{
		String host = configuracion.getHost();
		String puerto = configuracion.getPuerto();
		String starttls = configuracion.getStarttls();

        String sql = "INSERT INTO configuracionPop (host, puerto, starttls) VALUES ('" + host + "', '" + puerto + "', '" + starttls  + "')";
        
        try
        {
        	this.dbManager.ejecutarConsultaAbmSqlH2(sql, "Insertar", this.tipo);
        }
        catch(DAOException daoe)
        {
        	throw new DAOException(daoe.getMessage(),daoe);
        }
	}

	@Override
	public void modificarConfiguracionPop3(ConfiguracionPOP3 configuracion) throws DAOException 
	{
		String host = configuracion.getHost();
		String puerto = configuracion.getPuerto();
		String starttls = configuracion.getStarttls();

        String sql = "UPDATE configuracionPop set host = '" + host + "', puerto = '" + puerto +"', starttls = '" + starttls + "'";
        
        try
        {
        	this.dbManager.ejecutarConsultaAbmSqlH2(sql, "Modificar", this.tipo);
        }
        catch(DAOException daoe)
        {
        	throw new DAOException(daoe.getMessage(),daoe);
        }	
	}

	@Override
	public ConfiguracionPOP3 consultarConfiguracionPop3() throws DAOException 
	{
        String sql = "SELECT * FROM configuracionPop";
        
        try
        {
        	return ((ArrayList<ConfiguracionPOP3>) this.dbManager.ejecutarConsultaListadoSqlH2(sql, "Listar Uno", this.tipo)).get(0);

        }
        catch(DAOException daoe)
        {
        	throw new DAOException(daoe.getMessage()+"\nNo se puedo cargar la configuracion POP3",daoe);
        }    
        catch(IndexOutOfBoundsException ioobe)
        {
        	throw new DAOException("\nNo hay almacenada una configuracion POP3.\nIngrese a la configuracion para agregarla.",ioobe);
        }
	}

}
