package dao;

import java.util.ArrayList;
import exceptions.DAOException;
import modelo.Contacto;

public interface ContactoDAO 
{
	
	public void crearContacto(Contacto contacto) throws DAOException;
	
    public void borrarContacto(Contacto contacto) throws DAOException;
    
    public void modificarContacto(Contacto contacto) throws DAOException;
    
    public ArrayList<Contacto> listarContactos() throws DAOException;
    
    public Contacto consultarContacto(String mail) throws DAOException;
}
