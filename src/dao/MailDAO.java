package dao;

import java.util.ArrayList;
import exceptions.DAOException;
import modelo.Mail;

public interface MailDAO  
{

	public void crearMail(Mail mail) throws DAOException;
	
    public void modificarMail(Mail mail) throws DAOException;
    
    public ArrayList<Mail> listarMails() throws DAOException;   
}
