package dao;

import exceptions.DAOException;
import modelo.ConfiguracionSMTP;

public interface ConfiguracionSMTPDAO   
{

	public void crearConfiguracionSmtp(ConfiguracionSMTP configuracion) throws DAOException;
	
	public void modificarConfiguracionSmtp(ConfiguracionSMTP configuracion) throws DAOException;
    
    public ConfiguracionSMTP consultarConfiguracionSmtp() throws DAOException ;
    
}
