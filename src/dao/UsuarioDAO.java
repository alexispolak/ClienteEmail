package dao;

import java.util.ArrayList;
import exceptions.DAOException;
import modelo.Usuario;

public interface UsuarioDAO 
{

	public void crearUsuario(Usuario usuario) throws DAOException;
	
    public void borrarUsuario(Usuario usuario)  throws DAOException;
    
    public void modificarUsuario(Usuario usuario)  throws DAOException;
    
    public ArrayList<Usuario> listarUsuarios()  throws DAOException;
    
    public Usuario consultarUsuario(String mail)  throws DAOException;
    
}
