package dao;

import java.util.ArrayList;

import exceptions.DAOException;
import modelo.ConfiguracionPOP3;

public interface ConfiguracionPOP3DAO   
{

	public void crearConfiguracionPop3(ConfiguracionPOP3 configuracion) throws DAOException;
	
	public void modificarConfiguracionPop3(ConfiguracionPOP3 configuracion) throws DAOException;
    
    public ConfiguracionPOP3 consultarConfiguracionPop3() throws DAOException;
    
}
