package dataManager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import modelo.ConfiguracionPOP3;
import modelo.ConfiguracionSMTP;

public class DBManagerConfiguracionSMTP extends DBManager
{

	@Override
	public Object map(ResultSet resultSet) throws SQLException 
	{
		List listaRespuesta = new ArrayList();
		
		try
		{
			while(resultSet.next())
			{
				listaRespuesta.add(new ConfiguracionSMTP(resultSet.getString("host"), resultSet.getString("puerto"), resultSet.getString("starttls"), resultSet.getString("autorizacion")));
			}
			
			return listaRespuesta;
		}
		catch(SQLException sqle)
		{
			throw new SQLException("Error al mapear un CONFIGURACIO SMTP", sqle);		
		}
	}

}
