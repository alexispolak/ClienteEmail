package dataManager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import modelo.Contacto;
import modelo.Mail;

public class DBManagerMail extends DBManager
{

	@Override
	public Object map(ResultSet resultSet) throws SQLException
	{
		List listaRespuesta = new ArrayList();
		
		try
		{
			while(resultSet.next())
			{
				Mail mailDeBase = new Mail(resultSet.getInt("id"), resultSet.getString("origen"), resultSet.getString("destinatario"), resultSet.getString("asunto"), resultSet.getString("mensaje"));
				mailDeBase.setTag(resultSet.getString("tag"));
				listaRespuesta.add(mailDeBase);
			}
			
			return listaRespuesta;
		}
		catch(SQLException sqle)
		{
			throw new SQLException("Error al mapear un MAIL", sqle);		
		}
	}

}
