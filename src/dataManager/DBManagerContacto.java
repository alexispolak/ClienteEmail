package dataManager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import modelo.Contacto;

public class DBManagerContacto extends DBManager
{
	@Override
	public Object map(ResultSet resultSet) throws SQLException
	{
		List listaRespuesta = new ArrayList();
		
		try
		{
			while(resultSet.next())
			{
				listaRespuesta.add(new Contacto(resultSet.getInt("id"), resultSet.getString("nombre"), resultSet.getString("apellido"), resultSet.getString("direccion")));
			}
			
			return listaRespuesta;
		}
		catch(SQLException sqle)
		{
			throw new SQLException("Error al mapear un CONTACTO", sqle);		
		}
	}
}
