package dataManager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import modelo.Usuario;

public class DBManagerUsuario extends DBManager
{

	@Override
	public Object map(ResultSet resultSet) throws SQLException
	{
		List listaRespuesta = new ArrayList();
		
		try
		{
			while(resultSet.next())
			{
				listaRespuesta.add(new Usuario(resultSet.getString("nombre"), resultSet.getString("apellido"), resultSet.getString("usuario"),resultSet.getString("pass")));
			}
			
			return listaRespuesta;
		}
		catch(SQLException sqle)
		{
			throw new SQLException("Error al mapear un USUARIO", sqle);		
		}
	}

}
