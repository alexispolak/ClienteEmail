package dataManager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import modelo.ConfiguracionPOP3;
import modelo.Usuario;

public class DBManagerConfiguracionPop3 extends DBManager
{

	@Override
	public Object map(ResultSet resultSet) throws SQLException 
	{
		List listaRespuesta = new ArrayList();
		
		try
		{
			while(resultSet.next())
			{
				listaRespuesta.add(new ConfiguracionPOP3(resultSet.getString("host"), resultSet.getString("puerto"), resultSet.getString("starttls")));
			}
			
			return listaRespuesta;
		}
		catch(SQLException sqle)
		{
			throw new SQLException("Error al mapear un CONFIGURACION POP3", sqle);		
		}
	}

}
