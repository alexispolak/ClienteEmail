package dataManager;

import java.io.File;
import java.net.ConnectException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.h2.jdbc.JdbcSQLException;

import exceptions.DAOException;
import exceptions.DBCloseException;
import exceptions.DBCommitException;
import exceptions.DBConnectionException;
import exceptions.DBConsultaUpdateException;
import exceptions.DBException;
import exceptions.DBRollbackException;
import modelo.Contacto;
import modelo.Usuario;

public abstract class DBManager {
	
	private static final String DB_DRIVER = "org.h2.Driver";
	private static final String DB_BASE_URL = "jdbc:h2:tcp://localhost//{DIR}";
	private static final String DB_NAME = "/ClienteCorreo";
	private static final String DB_USERNAME = "sa";
	private static final String DB_PASSWORD = "";

	
	//Establece la conexion con la base de datos
	private Connection dameConexion() throws DBConnectionException 
	{
		Connection unaConexion = null;
		
		try 
		{
			//System.out.println("\n*- Comienza a realizar la conexion -*");
			
			Class.forName(DB_DRIVER);
			
			final String url = DB_BASE_URL.replace("{DIR}", obtenerUbicacionBase()) + DB_NAME;
			
			//System.out.println("URL JDBC: "+url);
			
			unaConexion = DriverManager.getConnection(url, DB_USERNAME, DB_PASSWORD);
			
			unaConexion.setAutoCommit(false);
			
			//System.out.println("*- Termino de conectar bien -*\n");
		} 
		catch(JdbcSQLException jdbce)
		{
			jdbce.printStackTrace();
			throw new DBConnectionException("Conexion Rota", jdbce);
		}
		catch (SQLException e) 
		{
			e.printStackTrace();
			throw new DBConnectionException("No se pudo establecer conexi�n en DBManager", e); //System.exit(0);
		}
		catch (ClassNotFoundException e) 
		{
			e.printStackTrace();
			throw new DBConnectionException("No se pudo encontrar el Driver en DBManager", e);
		}
		
		return unaConexion;
	}
	
	//Maneja el cierre de la conexion
	private void cerrarConexion(Connection unaConexion) throws DBCloseException 
	{
		try 
		{
			unaConexion.close();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			throw new DBCloseException("No se pudo cerrar la conexi�n de BDManager", e);
		}
		catch (NullPointerException npe) 
		{
			npe.printStackTrace();
			throw new DBCloseException("Inicie la base de datos para poder comenzar.", npe);
		}
	}
	
	//Se encarga de hacer el Rollback en la base de datos
	private void hacerRollback(Connection unaConexion)throws DBRollbackException 
	{
		try 
		{
			unaConexion.rollback();
		} 
		catch (SQLException e) 
		{	
			e.printStackTrace();
			throw new DBRollbackException("No se pudo hacer el RollBack de BDManager",e);
		}
	}
	
	//Se encarga de hacer el Commit
	private void hacerCommit(Connection unaConexion) throws DBCommitException
	{
		try
		{
			unaConexion.commit();
		}
		catch(SQLException sqle)
		{
			throw new DBCommitException("No se pudo hacer el Commit de DBManager", sqle);
		}
	}

	//Ejecutar consulta UPDATE
	private void ejecutarConsultaUpdate(Connection unaConexion, String sql) throws DBConsultaUpdateException
	{
		try
		{
			Statement s = unaConexion.createStatement();
			s.executeUpdate(sql);
		}
		catch(SQLException sqle)
		{
			throw new DBConsultaUpdateException("No se pudo hacer el ExecuteUpdate dentro de ejecutarConsultaUpdate() en el DBManager de la consulta:\n"+sql+"\nEsto se debe a un formato de mail no soportado.", sqle);
		}
	}

	//Ejecutar consulta QUERY
	private Object ejecutarConsultaQuery(Connection unaConexion, String sql) throws DBConsultaUpdateException
	{
		ResultSet resultado = null;
		
		try
		{
			Statement s = unaConexion.createStatement();
			resultado = s.executeQuery(sql);
			
			//Mapeo el resultset. Aca lo define el hijo.
			return map(resultado);
		}
		catch(SQLException sqle)
		{
			throw new DBConsultaUpdateException("No se pudo hacer el QUERY en el DBManager de la consulta:\n"+sql+"\n", sqle);
		}
	}
	
	//Ejecuta la consulta SQL en el motor H2 para CREAR, ELIMINAR y MODIFICAR
	public void ejecutarConsultaAbmSqlH2(String sql, String accion, String quienLoLlama) throws DAOException
	{
		Connection unaConexion = null;
		
        try
		{
        	//Creo conexion
			unaConexion = this.dameConexion();
			
			//Ejecuto la consulta
			this.ejecutarConsultaUpdate(unaConexion, sql);
			
			//Hago el commit
			this.hacerCommit(unaConexion);	
		}
		catch(DBConnectionException dbce)
		{
			throw new DAOException(dbce.getMessage(), dbce);
		}
        catch(DBConsultaUpdateException dbcue)
        {
        	try
			{
				this.hacerRollback(unaConexion);
			}
			catch(DBRollbackException dbe)
			{
				throw new DAOException(dbcue.getMessage()+"\n"+dbe.getMessage(), dbe);
			}
			
			throw new DAOException("Error cuando se quiere: "+accion+" el "+quienLoLlama+"\n"+dbcue.getMessage(), dbcue);
        }
        catch(DBCommitException dbcommite)
        {
        	throw new DAOException(dbcommite.getMessage(), dbcommite);
        }
        finally 
		{
        	//Cierro la conexion
			try
			{
				this.cerrarConexion(unaConexion);
			}
			catch(DBCloseException dbcierree)
			{
				throw new DAOException(dbcierree.getMessage(), dbcierree);
			}
			catch(NullPointerException npe)
			{
				throw new DAOException(npe.getMessage(), npe);
			}
		}
	}
	
	//Ejecuta la consulta SQL en el motor H2 para CREAR, ELIMINAR y MODIFICAR
	public Object ejecutarConsultaListadoSqlH2(String sql, String accion, String quienLoLlama) throws DAOException
	{
		Connection unaConexion = null;
		ResultSet respuesta = null;
		
        try
		{
        	//Creo conexion
			unaConexion = this.dameConexion();
			
			//Ejecuto la consulta
			return this.ejecutarConsultaQuery(unaConexion, sql);
			
		}
		catch(DBConnectionException dbce)
		{
			throw new DAOException(dbce.getMessage(), dbce);
		}
        catch(DBConsultaUpdateException dbcue)
        {
        	try
			{
				this.hacerRollback(unaConexion);
			}
			catch(DBRollbackException dbe)
			{
				throw new DAOException(dbcue.getMessage()+"\n"+dbe.getMessage(), dbe);
			}
			
			throw new DAOException("Error cuando se quiere: "+accion+" el "+quienLoLlama+"\n"+dbcue.getMessage(), dbcue);
        }
        finally 
		{
        	//Cierro la conexion
			try
			{
				this.cerrarConexion(unaConexion);
			}
			catch(DBCloseException dbcierree)
			{
				throw new DAOException(dbcierree.getMessage(), dbcierree);
			}
		}
	}
	
	
	//METODO DE GUIDO
	private static String obtenerUbicacionBase() {
		File currDir = new File("h2/base_de_datos/");
		return currDir.getAbsolutePath();
	}
	
	public abstract Object map(ResultSet rs)  throws SQLException;
}
