package exceptions;

public class DBCommitException  extends DBException
{
	public DBCommitException() {
   }

   public DBCommitException(String message) {
       super(message);
   }

   public DBCommitException(String message, Throwable cause) {
       super(message, cause);
   }

   public DBCommitException(Throwable cause) {
       super(cause);
   }

   public DBCommitException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
       super(message, cause, enableSuppression, writableStackTrace);
   }
}
