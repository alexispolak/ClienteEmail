package exceptions;

public class DBRollbackException  extends DBException
{
	public DBRollbackException() {
   }

   public DBRollbackException(String message) {
       super(message);
   }

   public DBRollbackException(String message, Throwable cause) {
       super(message, cause);
   }

   public DBRollbackException(Throwable cause) {
       super(cause);
   }

   public DBRollbackException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
       super(message, cause, enableSuppression, writableStackTrace);
   }
}
