package exceptions;

public class EnvioFallidoException  extends Exception {

    public EnvioFallidoException() {
    }

    public EnvioFallidoException(String message) {
        super(message);
    }

    public EnvioFallidoException(String message, Throwable cause) {
        super(message, cause);
    }

    public EnvioFallidoException(Throwable cause) {
        super(cause);
    }

    public EnvioFallidoException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
