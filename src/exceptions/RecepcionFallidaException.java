package exceptions;

public class RecepcionFallidaException  extends Exception {

    public RecepcionFallidaException() {
    }

    public RecepcionFallidaException(String message) {
        super(message);
    }

    public RecepcionFallidaException(String message, Throwable cause) {
        super(message, cause);
    }

    public RecepcionFallidaException(Throwable cause) {
        super(cause);
    }

    public RecepcionFallidaException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
