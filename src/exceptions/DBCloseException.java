package exceptions;

public class DBCloseException  extends DBException
{
	public DBCloseException() {
   }

   public DBCloseException(String message) {
       super(message);
   }

   public DBCloseException(String message, Throwable cause) {
       super(message, cause);
   }

   public DBCloseException(Throwable cause) {
       super(cause);
   }

   public DBCloseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
       super(message, cause, enableSuppression, writableStackTrace);
   }
}
