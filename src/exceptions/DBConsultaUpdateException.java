package exceptions;

public class DBConsultaUpdateException extends DBException
{
	public DBConsultaUpdateException() {
   }

   public DBConsultaUpdateException(String message) {
       super(message);
   }

   public DBConsultaUpdateException(String message, Throwable cause) {
       super(message, cause);
   }

   public DBConsultaUpdateException(Throwable cause) {
       super(cause);
   }

   public DBConsultaUpdateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
       super(message, cause, enableSuppression, writableStackTrace);
   }
}
