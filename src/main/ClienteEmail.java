package main;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import Pruebas.PruebaConfiguracion;
import Pruebas.PruebaContacto;
import Pruebas.PruebaEmail;
import Pruebas.PruebaGui;
import Pruebas.PruebaUsuarios;
import dao.ContactoDAO;
import dao.UsuarioDAO;
import dao.impl.ContactoDaoH2Impl;
import dao.impl.UsuarioDaoH2Impl;
import exceptions.DAOException;
import gui.GuiManager;
import modelo.AdministradorMails;
import modelo.Configuracion;
import modelo.ConfiguracionPOP3;
import modelo.ConfiguracionSMTP;
import modelo.Contacto;
import modelo.Mail;
import modelo.Usuario;

public class ClienteEmail 
{
	public static void main(String [ ] args)
	{
		ClienteEmail nuevoCliente = new ClienteEmail();
		AdministradorMails administradorPrincipal;
		Configuracion configuracionEnvioCorreo = null;
		Configuracion configuracionReciboCorreo = null;
		Usuario usuario = null;
		
		try 
		{
			configuracionEnvioCorreo = new ConfiguracionSMTP();
			configuracionReciboCorreo = new ConfiguracionPOP3();
			usuario = new Usuario();
		} 
		catch (DAOException e) 
		{
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		
		try
		{
			administradorPrincipal = new AdministradorMails(configuracionEnvioCorreo, configuracionReciboCorreo);
		
			Usuario unUsuario = new Usuario("Harambe", "McHarambe Jr", "pruebaMailLaboUno@gmail.com", "laboratorioUno");
			Contacto contacto1 = new Contacto("Alexis", "Polak", "alexis_polak@hotmail.com");
			Contacto contacto2 = new Contacto("Ramiro", "Colombo", "rc@hotmail.com");
			Contacto contacto3 = new Contacto("Sebastian", "Suarez", "ss@hotmail.com");
			Contacto contacto4 = new Contacto("Adrian", "Ingallina", "ai@hotmail.com");
			Contacto contacto5 = new Contacto("Lucas", "Mazalan", "alexispolak@gmail.com");
			
			administradorPrincipal.setUsuario(usuario);
			GuiManager.unicaInstancia().setAdministradorMails(administradorPrincipal);
			GuiManager.unicaInstancia().abrirAplicacion();

		
		} 
		catch (DAOException e) 
		{
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		catch (NullPointerException npe)
		{
			JOptionPane.showMessageDialog(null, "La aplicacion se cerrar�.");
		}
	}
	
	
	/*
	 * ConfiguracionSMTP("smtp.gmail.com", "25", "true", "true");
	 * ConfiguracionPOP3("pop.gmail.com", "995", "true");
	 * USUARIO: "pruebaMailLaboUno@gmail.com", PASS: "laboratorioUno"
	 * 
	 * 
	 * */
}
